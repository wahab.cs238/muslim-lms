import { Box, Container, Typography } from "@mui/material";
import React from "react";
import { comCss } from "./ComponentsCss";
import AccessTimeOutlinedIcon from "@mui/icons-material/AccessTimeOutlined";
import SignalCellularAltOutlinedIcon from "@mui/icons-material/SignalCellularAltOutlined";
import StarOutlinedIcon from "@mui/icons-material/StarOutlined";
import StarHalfOutlinedIcon from "@mui/icons-material/StarHalfOutlined";
import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import { compactFormat } from "cldr-compact-number/dist/cldr-compact-number.umd";

const Pagebanner = (props) => {
  const {
    title,
    subtitle,
    course_time,
    course_enroll,
    course_rating,
    course_expart,
    countAverageRating,
  } = props;
  const classes = comCss();
  return (
    <Box className={classes.page_banner}>
      <Container maxWidth="lg">
        <Typography
          variant="h2"
          component="h2"
          sx={{
            textAlign: { sm: "start" },
            marginBottom: "3rem",
            fontWeight: "700 !important",
            fontSize: {
              xs: "1.4rem !important",
              sm: "3rem !important",
            },
            color: "white",
          }}
        >
          {title}
        </Typography>
        {subtitle ? (
          <Typography
            variant="h3"
            component="h3"
            sx={{
              color: "white",
              fontSize: { xs: "1rem", sm: "1.3rem" },
            }}
          >
            {subtitle}
          </Typography>
        ) : (
          ""
        )}
        <Box className={classes.page_meta}>
          {course_time ? (
            <span className={classes.page_banner_icon}>
              <AccessTimeOutlinedIcon />
              {course_time}
            </span>
          ) : (
            ""
          )}

          {course_enroll ? (
            <span className={classes.page_banner_icon}>
              <PersonOutlineOutlinedIcon
                className={`${classes.page_banner_icon}`}
              />
              {
                compactFormat(course_enroll, "en", {
                  significantDigits: 1,
                  minimumFractionDigits: 1,
                  maximumFractionDigits: 2,
                })
                // 19.6K
              }{" "}
              Views
            </span>
          ) : (
            ""
          )}

          {course_rating ? (
            <span className={classes.page_banner_rating_icon}>
              {[1, 1, 1, 1, 1].map((item, idx) => {
                if (idx === course_rating) {
                  return;
                }
                return (
                  <svg
                    className="w-5 h-5 text-[#FDB241]"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                  </svg>
                );
              })}
              <p style={{ color: "#FFAA46", padding: "0px 5px 0px 5px" }}>
                4.5
              </p>
              <span>({course_rating})</span>
            </span>
          ) : (
            ""
          )}

          {course_expart ? (
            <span className={classes.page_banner_icon}>
              <SignalCellularAltOutlinedIcon
                className={`${classes.page_banner_icon}`}
              />
              {course_expart}
            </span>
          ) : (
            ""
          )}
        </Box>
      </Container>
    </Box>
  );
};

export default Pagebanner;
