import { Avatar, Box, Hidden, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import courses from "../../utils/data";
import axios from "axios";
// import Course from "../components/Course";

const AdminProfile = () => {
  const [adminInfo, setAdminInfo] = useState([]);

  useEffect(() => {
    const getAdminData = async () => {
      let response = await axios.get("http://localhost:3000/users/admin", {
        headers: {
          Authorization:
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2MjRkOWI5YTFiNGE2ZjY4Nzk2Njk0OSIsImlhdCI6MTcxMzc3Nzg5MSwiZXhwIjoxNzE0MDM3MDkxfQ.0yuQ4UvFO_ho_uel9PTpNcXYTfMgonVH2tFkewlNbkg",
        },
      });
      setAdminInfo(response?.data);
      console.log(adminInfo);
    };
    getAdminData();
  }, []);

  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        {/* container for two rows */}
        <Box
          sx={{
            mt: { xs: 0, sm: "4rem" },

            maxWidth: "1024px",
            display: "flex",
            flexDirection: { xs: "column-reverse", sm: "row" },
            gap: "3rem",
          }}
        >
          {/* row 1, name and other details */}
          <Box
            sx={{
              marginBottom: "7rem",
              px: { xs: "20px", sm: "32px" },
            }}
          >
            <Typography
              variant="h4"
              sx={{
                fontWeight: 700,

                fontSize: "1.4rem",
                color: "gray",
              }}
            >
             {adminInfo?.user?.role}
            </Typography>
            <Box>
              <Typography
                sx={{
                  fontWeight: 700,
                  fontSize: { xs: "2rem", sm: "3rem" },
                  textTransform:"capitalize"
                }}
              >
                 {adminInfo?.user?.name}
              </Typography>
              <Typography
                sx={{
                  fontWeight: "400",
                  fontSize: "1.2rem",
                }}
              >
                  {adminInfo?.user?.bio}
              </Typography>

              <Box
                sx={{
                  display: "flex",
                  gap: "3rem",
                  mt: "2rem",
                }}
              >
                <Stack>
                  <Typography
                    variant="h4"
                    sx={{
                      fontWeight: 700,
                      fontSize: "1rem",
                      color: "gray",
                    }}
                  >
                    Total Courses
                  </Typography>{" "}
                  <Typography
                    sx={{
                      fontWeight: "600",
                      fontSize: "1.2rem",
                    }}
                  >
                    {adminInfo?.user?.courses?.length}
                  </Typography>
                </Stack>
                <Stack>
                  <Typography
                    variant="h4"
                    sx={{
                      fontWeight: 700,
                      fontSize: "1rem",
                      color: "gray",
                    }}
                  >
                    Total Views
                  </Typography>{" "}
                  <Typography
                    sx={{
                      fontWeight: "600",
                      fontSize: "1.2rem",
                    }}
                  >
                    {adminInfo?.totalViews}
                  </Typography>
                </Stack>
              </Box>
            </Box>

            {/* About me */}
            <Box
              sx={{
                mt: "1.5rem",
              }}
            >
              <Typography
                variant="h4"
                sx={{
                  fontWeight: 700,
                  mt: "2rem",
                  mb: "1rem",
                  color: "#754ffe",
                  fontSize: "1.4rem",
                }}
              >
                About Me
              </Typography>
              <Typography
                variant="body1"
                sx={{
                  maxWidth: { xs: "auto", sm: "43vw" },
                }}
              >
                {adminInfo?.user?.about}
              </Typography>
            </Box>
            {/* about me end */}
            {/* <Typography
              variant="h4"
              sx={{
                fontWeight: 700,
                mt: "3rem",
                mb: "2rem",
                fontSize: "1.4rem",
              }}
            >
              My Courses ({courses.length})
            </Typography> */}

            {/* <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: { xs: "center", sm: "start" },
                gap: "2rem",
              }}
            >
              {courses.map((item, idx) => {
                if (idx >= 5) {
                  return;
                }
                return <Course course={item} key={idx} />;
              })}
            </Box> */}

            <Hidden smUp>
              <Stack
                sx={{
                  justifyContent: "center",
                  mt: "3rem",
                  gap: ".5rem",
                }}
              >
                {[1, 1, 1].map(() => {
                  return (
                    <Link to={"/"} target="_blank">
                      <Box
                        sx={{
                          display: "flex",
                          px: "3rem",
                          border: "1px solid #754ffe",
                          height: "3.2rem",
                          justifyContent: "center",
                          alignItems: "center",
                          gap: ".4rem",
                          "&:hover": {
                            backgroundColor: "#edededbd",
                          },
                        }}
                      >
                        <Typography
                          sx={{
                            textTransform: "capitalize",
                            fontWeight: "500",
                          }}
                        >
                          twitter
                        </Typography>
                      </Box>
                    </Link>
                  );
                })}
              </Stack>
            </Hidden>
          </Box>

          {/* row 2, image and links */}

          <Box
            sx={{
              px: { xs: "20px", sm: "32px" },
              marginTop: { xs: "3rem", sm: "0" },
            }}
          >
            <Avatar
              alt="Remy Sharp"
              src="https://scontent.fkdh2-1.fna.fbcdn.net/v/t39.30808-1/419242635_2102495056770043_997954213065734750_n.jpg?stp=dst-jpg_s200x200&_nc_cat=107&ccb=1-7&_nc_sid=5f2048&_nc_ohc=TuWFwryZGdAAb7EdT-C&_nc_ht=scontent.fkdh2-1.fna&oh=00_AfAYeSEzKkcdfprbxysmwZcqfGwXTDFD2or7zOEoSzwh1A&oe=66217233"
              sx={{ width: 200, height: 200, border: "3px solid  #754ffe" }}
            />

            <Hidden smDown>
              <Stack
                sx={{
                  justifyContent: "center",
                  mt: "3rem",
                  gap: ".5rem",
                }}
              >
                {[1, 1, 1].map(() => {
                  return (
                    <Link to={"/"} target="_blank">
                      <Box
                        sx={{
                          display: "flex",
                          px: "3rem",
                          border: "1px solid #754ffe",
                          height: "3.2rem",
                          justifyContent: "center",
                          alignItems: "center",
                          gap: ".4rem",
                          "&:hover": {
                            backgroundColor: "#edededbd",
                          },
                        }}
                      >
                        <Typography
                          sx={{
                            textTransform: "capitalize",
                            fontWeight: "500",
                          }}
                        >
                          twitter
                        </Typography>
                      </Box>
                    </Link>
                  );
                })}
              </Stack>
            </Hidden>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default AdminProfile;
