import { useEffect, useState, useRef } from "react";
import Select from "react-select";
import makeAnimated from "react-select/animated";

import ErrorMsg from "../../ErrorMsg/ErrorMsg";
import LoadingComponent from "../../LoadingComp/LoadingComponent";
import SuccessMsg from "../../SuccessMsg/SuccessMsg";
import { Link } from "react-router-dom";
import { Box, Button } from "@mui/material";

import { AiOutlinePlusCircle } from "react-icons/ai";
import { BsListCheck } from "react-icons/bs";
import { TbEdit } from "react-icons/tb";
import { AiOutlineDelete } from "react-icons/ai";
import { EllipsisVerticalIcon } from "@heroicons/react/24/outline";
import { BsImages } from "react-icons/bs";

import ReactQuill from "react-quill";

import "react-quill/dist/quill.snow.css";

//animated components for react-select
const animatedComponents = makeAnimated();

// yup & formik
import * as yup from "yup";
import { useFormik } from "formik";
import TextError from "../../../components/TextError";

// yup
const RegisterSchema = yup.object().shape({
  title: yup.string().required(),
  description: yup.string().required(),
  category: yup.string().required(),
  image: yup.string().required(),
});

// redux
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

// store
import {
  createArticle,
  selectIsLoading,
} from "../../../store/features/article/articleSlice";

export default function AddArticle() {
  // redux
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isLoading = useSelector(selectIsLoading);

  const [value, setValue] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const quillRef = useRef(null);
  const [descriptionValue, setDescripitonValue] = useState("");

  const initialValues = {
    title: "",
    description: "",
    category: "",
  };

  const {
    values,
    errors: formError,
    handleBlur,
    handleChange,
  } = useFormik({
    initialValues,
    validationSchema: RegisterSchema,
    onSubmit: async () => {
      const data = {
        title: values.title,
        description: descriptionValue,
        category,
        image: imageUrl,
      };
      await dispatch(createArticle(data));
    },
  });
  console.log(values.title, descriptionValue, values.category, imageUrl);
  const handleOnChange = (e) => {
    const quill = quillRef.current.getEditor();
    const html = quill.root.innerHTML;
    setDescripitonValue(html);
  };

  const handleDeleteVideo = () => {
    setImageUrl("");
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    const imageUrl = URL.createObjectURL(file);
    setImageUrl(imageUrl);
    console.log(imageUrl);
  };

  //---form data---
  const [formData, setFormData] = useState({
    name: "",
    description: "",
    category: "",
    sizes: "",
    brand: "",
    colors: "",
    images: "",
    price: "",
    totalQty: "",
  });

  const toolbarOptions = [
    ["bold", "italic", "underline"], // toggled buttons
    ["blockquote", "code-block"],
    ["link", "image", "video", "formula"],

    [{ list: "ordered" }, { list: "bullet" }, { list: "check" }],
    [{ script: "sub" }, { script: "super" }], // superscript/subscript
    [{ direction: "rtl" }], // text direction

    [{ size: ["small", false, "large", "huge"] }], // custom dropdown
    [{ header: [1, 2, 3, 4, 5, 6, false] }],

    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ align: [] }],

    ["clean"], // remove formatting button
  ];

  let error, isAdded;

  //onChange

  return (
    <>
      {error && <ErrorMsg message={error?.message} />}
      {isAdded && <SuccessMsg message="Product Added Successfully" />}
      <Box>
        <section class="bg-white dark:bg-gray-900">
          <div class="py-8 px-10 lg:py-16">
            <h2 class="mb-4 text-xl font-bold text-gray-900 dark:text-white">
              Add New Article
            </h2>
            <form action="#">
              <Box
                sx={{
                  display: "flex",
                  gap: "3rem",
                  flexDirection: { xs: "column", md: "row" },
                }}
              >
                <Box
                  className="w-full"
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "2rem",
                  }}
                >
                  <div>
                    <label
                      for="name"
                      class="block text-sm mb-2 font-medium text-gray-900 dark:text-white"
                    >
                      Article Title
                    </label>
                    <input
                      type="text"
                      name="title"
                      onCreateTodo
                      id="name"
                      class="bg-indigo-100 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-indigo-600 focus:border-indigo-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                      placeholder="Type Course name"
                      required=""
                      value={values?.title}
                      onBlur={handleBlur}
                      onChange={handleChange}
                    />
                    {formError.title ? (
                      <TextError error={formError.title} />
                    ) : null}
                  </div>

                  <div>
                    <label
                      for="category"
                      class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Category
                    </label>
                    <select
                      id="category"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      name="category"
                      class="bg-indigo-100 border border-gray-300 text-gray-900 text-sm rounded-md  focus:ring-primary-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                    >
                      <option value="">Select category</option>
                      <option value={"TV"}>TV/Monitors</option>
                      <option value={"PC"}>PC</option>
                      <option value={"Gaming"}>Gaming/Console</option>
                      <option value={"Phones"}>Phones</option>
                    </select>
                    {formError?.category ? (
                      <TextError error={formError.category} />
                    ) : null}
                  </div>

                  <div className="w-full">
                    <label
                      for="description"
                      class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Description
                    </label>
                    <ReactQuill
                      style={{ color: "white" }}
                      ref={quillRef}
                      modules={{
                        toolbar: toolbarOptions,
                      }}
                      theme="snow"
                      value={descriptionValue}
                      onChange={handleOnChange}
                    />
                  </div>

                  {/*  */}
                  {/*  */}
                  {/*  */}
                  <label
                    for="dropzone-file"
                    className={`flex flex-col items-center justify-center w-full border border-gray-300 border-dashed cursor-pointer bg-gray-50 ${
                      imageUrl ? "0" : "py-16"
                    } `}
                  >
                    {imageUrl ? (
                      <Box
                        // className="bg-indigo-100 px-6 pb-4"
                        sx={{
                          borderRadius: "4px",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center",
                          }}
                        ></Box>

                        <Box
                          sx={{
                            display: "flex",
                            flexDirection: "column",
                            gap: ".6rem",
                          }}
                        >
                          <Button
                            sx={{
                              color: "red",
                              "&:hover": {
                                color: "red",
                              },
                              alignSelf: "end",
                              mt: "1rem",
                            }}
                            onClick={handleDeleteVideo}
                          >
                            <AiOutlineDelete
                              style={{
                                width: "1.22rem",
                                height: "1.22rem",
                              }}
                            />
                          </Button>

                          <img
                            src={imageUrl}
                            class="w-full rounded border bg-white p-1 dark:border-neutral-700 dark:bg-neutral-800"
                            alt="..."
                          />

                          {/* <img src={imageUrl} width={"100%"} /> */}
                        </Box>
                      </Box>
                    ) : (
                      <>
                        <div class="mb-3 flex items-center justify-center">
                          <BsImages
                            style={{
                              width: "3rem",
                              height: "3rem",
                              marginBottom: "1rem",
                              color: "#754ffe",
                            }}
                          />
                        </div>
                        <h2 class="text-center text-gray-400   text-xs font-normal leading-4 mb-1">
                          image smaller than 15mb
                        </h2>
                        <h4 class="text-center text-gray-900 text-sm font-medium leading-snug">
                          Drag and Drop your file here or
                        </h4>
                        <input
                          type="file"
                          accept="image/*"
                          onChange={handleFileChange}
                          id="dropzone-file"
                          class="hidden"
                          required
                        />
                      </>
                    )}
                  </label>
                </Box>
              </Box>
              <button
                type="submit"
                class="inline-flex  items-center px-5 py-2.5 sm:mt-6 text-sm font-medium text-center text-white bg-indigo-700 rounded-lg focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800"
                style={{ backgroundColor: "#754ffe" }}
              >
                Add Article
              </button>
            </form>
          </div>
        </section>
      </Box>
    </>
  );
}
