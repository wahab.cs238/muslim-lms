import React, { Fragment, useEffect, useState } from "react";
import { Dialog, Menu, Transition } from "@headlessui/react";
import { Link, NavLink, Outlet, useLocation } from "react-router-dom";
import {
  Bars3CenterLeftIcon,
  BellIcon,
  ClockIcon,
  CogIcon,
  CreditCardIcon,
  DocumentChartBarIcon,
  HomeIcon,
  QuestionMarkCircleIcon,
  ScaleIcon,
  ShieldCheckIcon,
  UserGroupIcon,
  XMarkIcon,
} from "@heroicons/react/24/outline";
import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  useDisclosure,
  Input,
  ChakraProvider,
  Hide,
  Show,
} from "@chakra-ui/react";
import { Box, Hidden, Button } from "@mui/material";
import AdminProfile from "./AdminProfile";
import FetchFromApi from "../../utils/FetchFromApi";

// icons

import { PiVideoThin } from "react-icons/pi";
import { MdOutlineVideoSettings } from "react-icons/md";
import { MdVideoLibrary } from "react-icons/md";
import { GrArticle } from "react-icons/gr";
import { TbCategory2 } from "react-icons/tb";
import { HiOutlineHome } from "react-icons/hi";
import { AiOutlineHome } from "react-icons/ai";
import { TbCategoryPlus } from "react-icons/tb";
import { HiPencilSquare } from "react-icons/hi2";
import { GrUserSettings } from "react-icons/gr";

import { useDispatch, useSelector } from "react-redux";
import {
  getAdminProfile,
  selectAdminProfile,
  selectIsLoading,
} from "../../store/auth/admin/adminSlice";

const dashboardLinks = [
  {
    name: "Dashboard",
    href: "",
    icon: () => (
      <AiOutlineHome size={"1.5rem"} style={{ marginRight: ".6rem" }} />
    ),
    current: true,
  },
  {
    name: "Settings",
    href: "settings",
    icon: () => (
      <GrUserSettings size={"1.5rem"} style={{ marginRight: ".6rem" }} />
    ),
    current: true,
  },
];

const courseLinks = [
  {
    name: "Add Course",
    href: "add-course",
    icon: () => (
      <MdVideoLibrary size={"1.5rem"} style={{ marginRight: ".6rem" }} />
    ),
    current: false,
  },
  {
    name: "Manage Courses",
    href: "manage-courses",
    icon: () => (
      <MdOutlineVideoSettings
        size={"1.5rem"}
        style={{ marginRight: ".6rem" }}
      />
    ),
  },
];

const articleLinks = [
  {
    name: "Add Article",
    href: "add-article",
    icon: () => <GrArticle size={"1.5rem"} style={{ marginRight: ".6rem" }} />,
  },
  {
    name: "Manage Articles",
    href: "manage-articles",
    icon: () => (
      <HiPencilSquare size={"1.5rem"} style={{ marginRight: ".6rem" }} />
    ),
  },
];

const categoriesLinks = [
  {
    name: "Add Category",
    href: "add-category",
    icon: () => (
      <TbCategory2 size={"1.5rem"} style={{ marginRight: ".6rem" }} />
    ),
  },
  {
    name: "Manage Category",
    href: "manage-category",
    icon: TbCategoryPlus,
  },
];

const couponsLinks = [];
function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Example() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isAdminRoute, setIsAdminRoute] = useState(false);

  //redux
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading);
  // const AdminProfile = useSelector(selectAdminProfile);

  const btnRef = React.useRef();
  const location = useLocation();

  useEffect(() => {
    dispatch(getAdminProfile);
    setIsAdminRoute(window.location.pathname === "/admin" ? true : false);
  }, [location.pathname]);

  return (
    <>
      <ChakraProvider>
        <Drawer
          isOpen={isOpen}
          placement="left"
          onClose={onClose}
          finalFocusRef={btnRef}
        >
          <DrawerOverlay />
          <DrawerContent
            style={{
              width: "60%",
            }}
          >
            <Box
              sx={{
                backgroundColor: "rgb(22 78 99)",
                pb: "2rem",
                height: "100%",
                width: "100%",
              }}
              style={{
                overflow: "auto",
              }}
            >
              <DrawerCloseButton
                top={"5.5rem"}
                right={"1.5rem"}
                bgColor={"white"}
                f
              />
              <Box sx={{ marginTop: "5rem" }} />

              <nav
                className="mt-16 flex flex-1 flex-col items-start pl-6 divide-y divide-cyan-800"
                style={{ gap: "1rem" }}
                aria-label="Sidebar"
              >
                {/* mobile links */}
                <Box sx={{ pt: "2rem" }} />
                {/* courses links  */}
                <div className="mt-1 pt-1">
                  <Box
                    className="space-y-1 px-2"
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    {dashboardLinks.map((item) => (
                      <NavLink
                        key={item.name}
                        to={item.href}
                        className="group flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                        style={{
                          gap: ".4rem",
                          height: "2.7rem",
                          minWidth: "165px",
                        }}
                      >
                        <item.icon
                          className="h-6 w-6 text-cyan-200"
                          aria-hidden="true"
                        />
                        {item.name}
                      </NavLink>
                    ))}
                  </Box>
                </div>
                {/* courses links  */}
                <div className="mt-1 pt-1">
                  <Box
                    className="space-y-1 px-2"
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    {courseLinks.map((item) => (
                      <NavLink
                        key={item.name}
                        to={item.href}
                        className="group  flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                        style={{
                          gap: ".4rem",
                          height: "2.7rem",
                        }}
                      >
                        <item.icon
                          className="h-6 w-6 text-cyan-200"
                          aria-hidden="true"
                        />
                        {item.name}
                      </NavLink>
                    ))}
                  </Box>
                </div>
                {/* articles links */}
                <div className="mt-1 pt-1">
                  <Box
                    className="space-y-1 px-2"
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    {articleLinks.map((item) => (
                      <NavLink
                        key={item.name}
                        to={item.href}
                        className="group  flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                        style={{
                          gap: ".4rem",
                          height: "2.7rem",
                        }}
                      >
                        <item.icon
                          className="h-6 w-6 text-cyan-200"
                          aria-hidden="true"
                        />
                        {item.name}
                      </NavLink>
                    ))}
                  </Box>
                </div>

                {/* category links */}
                <div className="mt-1 pt-1">
                  <Box
                    className="space-y-1 px-2"
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    {categoriesLinks.map((item) => (
                      <NavLink
                        key={item.name}
                        to={item.href}
                        className="group  flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                        style={{
                          gap: ".4rem",
                          height: "2.7rem",
                        }}
                      >
                        <item.icon
                          className="h-6 w-6 text-cyan-200"
                          aria-hidden="true"
                        />
                        {item.name}
                      </NavLink>
                    ))}
                  </Box>
                </div>
              </nav>
            </Box>
          </DrawerContent>
        </Drawer>
      </ChakraProvider>

      <div className="min-h-full ">
        {/* Static sidebar for desktop */}
        <div className="hidden lg:fixed lg:inset-y-0 lg:flex lg:w-64 lg:flex-col">
          {/* Sidebar component, swap this element with another sidebar if you like */}

          <div className="flex flex-grow flex-col  bg-cyan-900 pt-5 pb-4 overflow-y-auto">
            <nav
              className="mt-5 flex flex-1 flex-col divide-y divide-cyan-800"
              style={{ gap: "1rem" }}
              aria-label="Sidebar"
            >
              <Box sx={{ pt: "2rem" }} />
              {/* courses links desktop */}
              <div className="mt-1 pt-1">
                <Box
                  className="space-y-1 px-2"
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  {dashboardLinks.map((item) => (
                    <NavLink
                      key={item.name}
                      to={item.href}
                      className="group flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                      style={{
                        gap: ".4rem",
                        height: "2.7rem",
                        minWidth: "165px",
                      }}
                    >
                      <item.icon
                        className="h-6 w-6 text-cyan-200"
                        aria-hidden="true"
                      />
                      {item.name}
                    </NavLink>
                  ))}
                </Box>
              </div>
              {/* courses links desktop */}
              <div className="mt-1 pt-1">
                <Box
                  className="space-y-1 px-2"
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  {courseLinks.map((item) => (
                    <NavLink
                      key={item.name}
                      to={item.href}
                      className="group  flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                      style={{
                        gap: ".4rem",
                        height: "2.7rem",
                      }}
                    >
                      <item.icon
                        className="h-6 w-6 text-cyan-200"
                        aria-hidden="true"
                      />
                      {item.name}
                    </NavLink>
                  ))}
                </Box>
              </div>
              {/* articles links */}
              <div className="mt-1 pt-1">
                <Box
                  className="space-y-1 px-2"
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  {articleLinks.map((item) => (
                    <NavLink
                      key={item.name}
                      to={item.href}
                      className="group  flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                      style={{
                        gap: ".4rem",
                        height: "2.7rem",
                      }}
                    >
                      <item.icon
                        className="h-6 w-6 text-cyan-200"
                        aria-hidden="true"
                      />
                      {item.name}
                    </NavLink>
                  ))}
                </Box>
              </div>

              {/* category links */}
              <div className="mt-1 pt-1">
                <Box
                  className="space-y-1 px-2"
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  {categoriesLinks.map((item) => (
                    <NavLink
                      key={item.name}
                      to={item.href}
                      className="group  flex items-center rounded-md px-2 py-2 text-sm font-medium leading-6 text-cyan-100 hover:bg-cyan-600 hover:text-white"
                      style={{
                        gap: ".4rem",
                        height: "2.7rem",
                      }}
                    >
                      <item.icon
                        className="h-6 w-6 text-cyan-200"
                        aria-hidden="true"
                      />
                      {item.name}
                    </NavLink>
                  ))}
                </Box>
              </div>
            </nav>
          </div>
        </div>

        <div className="flex flex-1 flex-col lg:pl-64">
          <Hidden lgUp>
            <Button
              variant="contained"
              disableElevation
              ref={btnRef}
              onClick={onOpen}
              mb={"1rem"}
              sx={{
                backgroundColor: "#754ffe",
                padding: "0",
                px: ".4rem",
                py: ".6rem",
                "&:hover": {
                  backgroundColor: "#754ffe",
                },
                mt: "1rem",
              }}
            >
              <Bars3CenterLeftIcon className="h-6 w-6" aria-hidden="true" />
            </Button>
          </Hidden>

          <main className="w-full pb-8">
            <Box sx={{ pt: "6rem" }} />
            {isAdminRoute ? <AdminProfile /> : <Outlet />}

            {/* content */}
          </main>
        </div>
      </div>
    </>
  );
}
