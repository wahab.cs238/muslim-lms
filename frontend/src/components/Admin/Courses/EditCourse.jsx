import { useEffect, useRef, useState } from "react";

import { Link, NavLink, useNavigate } from "react-router-dom";
import { Box, Button, Typography } from "@mui/material";

import ReactQuill from "react-quill";

import "react-quill/dist/quill.snow.css";

// icons
import { AiOutlinePlusCircle } from "react-icons/ai";
import { TbEdit } from "react-icons/tb";
import { AiOutlineDelete } from "react-icons/ai";
import { BsImages } from "react-icons/bs";

//    ---------store----------

//Course
import {
  Video_UpdateCourse,
  selectIsLoading,
} from "../../../store/features/video/courses/videoCoursesSlice";

// chapter Or Section
import {
  Video_DeleteSection,
  selectIsLoading as Video_DeleteSection_IsLoading,
} from "../../../store/features/video/section/videoSectionSlice";

// redux
import { useDispatch, useSelector } from "react-redux";

// yup & formik
import * as yup from "yup";
import { useFormik } from "formik";
import TextError from "../../../components/TextError";

// yup
const RegisterSchema = yup.object().shape({
  title: yup.string().required(),
  description: yup.string().required(),
  category: yup.string().required(),
  image: yup.string().required(),
});

export default function EditCourse() {
  // redux
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const isLoading = useSelector(selectIsLoading);

  // state
  const quillRef = useRef(null);
  const [descriptionValue, setDescripitonValue] = useState("");
  const [ImageUrl, setImageUrl] = useState("");
  const [Chapters, setChapters] = useState([
    { title: "Chapter1" },
    { title: "Chapter2" },
  ]);

  // hooks
  const initialValues = {
    title: "",
    category: "",
  };

  const {
    values,
    errors: formError,
    handleBlur,
    handleChange,
  } = useFormik({
    initialValues,
    validationSchema: RegisterSchema,
    onSubmit: async () => {
      const { title, description, category, image } = values;
      const data = {
        title,
        description: descriptionValue,
        category,
        image: ImageUrl,
      };
      await Video_UpdateCourse(data);
    },
  });

  const toolbarOptions = [
    ["bold", "italic", "underline"], // toggled buttons
    ["blockquote", "code-block"],
    ["link", "image", "video", "formula"],

    [{ list: "ordered" }, { list: "bullet" }, { list: "check" }],
    [{ script: "sub" }, { script: "super" }], // superscript/subscript
    [{ direction: "rtl" }], // text direction

    [{ size: ["small", false, "large", "huge"] }], // custom dropdown
    [{ header: [1, 2, 3, 4, 5, 6, false] }],

    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ align: [] }],

    ["clean"], // remove formatting button
  ];

  // handlers

  const handleDeleteImage = () => {
    setImageUrl("");
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    const imageUrl = URL.createObjectURL(file);
    setImageUrl(imageUrl);
  };

  const handleOnChange = (e) => {
    const quill = quillRef.current.getEditor();
    const html = quill.root.innerHTML;
    setDescripitonValue(html);
  };

  // delete Chapter
  const deleteChapter = async (id) => {
    await Video_DeleteSection(id);
  };

  return (
    <>
      {/* {error && <ErrorMsg message={error?.message} />} */}
      {/* {isAdded && <SuccessMsg message="Product Added Successfully" />} */}
      <Box>
        <section class="bg-white dark:bg-gray-900">
          <div class="py-8 px-10 lg:py-16">
            <h2 class="mb-4 text-xl font-bold text-gray-900 dark:text-white">
              Edit this Course
            </h2>
            <form action="#">
              <Box
                sx={{
                  display: "flex",
                  gap: "3rem",
                  flexDirection: { xs: "column", md: "row" },
                }}
              >
                <Box
                  className="w-full"
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "2rem",
                  }}
                >
                  <div>
                    <label
                      for="name"
                      class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Course Title
                    </label>
                    <input
                      type="text"
                      name="title"
                      onCreateTodo
                      id="name"
                      class="bg-indigo-100 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-indigo-600 focus:border-indigo-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                      placeholder="Type Course name"
                      required=""
                      value={values?.title}
                      onBlur={handleBlur}
                      onChange={handleChange}
                    />
                    {formError.title ? (
                      <TextError error={formError.title} />
                    ) : null}
                  </div>

                  <div>
                    <label
                      for="category"
                      class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Category
                    </label>
                    <select
                      id="category"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      name="category"
                      class="bg-indigo-100 border border-gray-300 text-gray-900 text-sm rounded-md  focus:ring-primary-500 focus:border-indigo-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                    >
                      <option value="">Select category</option>
                      <option value={"TV"}>TV/Monitors</option>
                      <option value={"PC"}>PC</option>
                      <option value={"Gaming"}>Gaming/Console</option>
                      <option value={"Phones"}>Phones</option>
                    </select>
                    {formError?.category ? (
                      <TextError error={formError.category} />
                    ) : null}
                  </div>

                  <div className="w-full">
                    <label
                      for="description"
                      class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                    >
                      Description
                    </label>
                    <ReactQuill
                      style={{ color: "white" }}
                      ref={quillRef}
                      modules={{
                        toolbar: toolbarOptions,
                      }}
                      theme="snow"
                      value={descriptionValue}
                      onChange={handleOnChange}
                    />
                    {descriptionValue === "" ? (
                      <TextError error={"Description Required"} />
                    ) : null}
                  </div>

                  {/* upload image */}

                  <label
                    for="dropzone-file"
                    className={`flex flex-col items-center justify-center w-full border border-gray-300 border-dashed cursor-pointer bg-gray-50 ${
                      ImageUrl ? "0" : "py-16"
                    } `}
                  >
                    {ImageUrl ? (
                      <Box
                        // className="bg-indigo-100 px-6 pb-4"
                        sx={{
                          borderRadius: "4px",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center",
                          }}
                        ></Box>

                        <Box
                          sx={{
                            display: "flex",
                            flexDirection: "column",
                            gap: ".6rem",
                          }}
                        >
                          <Button
                            sx={{
                              color: "red",
                              "&:hover": {
                                color: "red",
                              },
                              alignSelf: "end",
                              mt: "1rem",
                            }}
                            onClick={handleDeleteImage}
                          >
                            <AiOutlineDelete
                              style={{
                                width: "1.22rem",
                                height: "1.22rem",
                              }}
                            />
                          </Button>

                          <img
                            src={ImageUrl}
                            class="w-full rounded border bg-white p-1 dark:border-neutral-700 dark:bg-neutral-800"
                            alt="..."
                          />

                          {/* <img src={imageUrl} width={"100%"} /> */}
                        </Box>
                      </Box>
                    ) : (
                      <>
                        <div class="mb-3 flex items-center justify-center">
                          <BsImages
                            style={{
                              width: "3rem",
                              height: "3rem",
                              marginBottom: "1rem",
                              color: "#754ffe",
                            }}
                          />
                        </div>
                        <h2 class="text-center text-gray-400   text-xs font-normal leading-4 mb-1">
                          image smaller than 15mb
                        </h2>
                        <h4 class="text-center text-gray-900 text-sm font-medium leading-snug">
                          Drag and Drop your file here or
                        </h4>
                        <input
                          type="file"
                          accept="image/*"
                          onChange={handleFileChange}
                          id="dropzone-file"
                          class="hidden"
                          required
                        />
                      </>
                    )}
                  </label>
                  {/* )}   */}
                  {/*  */}
                </Box>

                {/* Chapters block */}

                <div className="w-full">
                  <label
                    for="description"
                    style={{
                      display: "flex",
                      alignItems: "center",
                      gap: ".4rem",
                    }}
                    class="block text-sm mb-2 font-medium text-gray-900 dark:text-white"
                  >
                    Course Chapter's
                  </label>

                  <Box
                    className="bg-indigo-100 px-6 pb-4"
                    sx={{
                      borderRadius: "4px",
                    }}
                  >
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-between",
                        py: "1rem",
                        alignItems: "center",
                      }}
                    >
                      <label
                        for="description"
                        class="block text-xs font-medium text-gray-900 dark:text-black text-end"
                      >
                        Course Chapter's
                      </label>

                      <Link to="/admin/add-chapter">
                        <button
                          type="submit"
                          class="inline-flex  items-center px-5 py-1 sm:mt-6 text-sm font-medium text-center text-white bg-indigo-700 rounded-sm focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800"
                          style={{
                            alignItems: "center",
                            backgroundColor: "#754ffe",
                            cursor: "pointer",
                          }}
                        >
                          <label
                            for="description"
                            style={{
                              display: "flex",
                              alignItems: "center",
                              gap: ".4rem",
                              color: "white",
                              cursor: "pointer",
                            }}
                            class="block text-xs font-medium text-gray-900 dark:text-white text-end"
                          >
                            <AiOutlinePlusCircle
                              style={{ width: ".9rem", height: ".9rem" }}
                            />
                            Add Chapter's
                          </label>
                        </button>
                      </Link>
                    </Box>

                    {/* course chapters show here */}
                    <Box
                      sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: ".6rem",
                      }}
                    >
                      {Chapters?.map((chapter) => {
                        return (
                          <Box
                            sx={{
                              backgroundColor: "#c0e0ff",
                              px: ".8rem",
                              py: ".3rem",
                              borderRadius: "4px",
                              display: "flex",
                              alignItems: "center",
                              justifyContent: "space-between",
                            }}
                          >
                            <Typography
                              variant="h5"
                              sx={{
                                fontSize: ".9rem",
                                fontWeight: "500",
                                textTransform: "capitalize",
                              }}
                            >
                              {"introduction to the course of web development".slice(
                                0,
                                50
                              )}
                            </Typography>
                            <Box display={"flex"}>
                              <Button
                                component={NavLink}
                                to="/admin/edit-chapter/121231"
                                sx={{
                                  color: "#754ffe",

                                  "&:hover": {
                                    color: "#754ffe",
                                  },
                                }}
                              >
                                <TbEdit
                                  style={{
                                    width: "1.22rem",
                                    height: "1.22rem",
                                  }}
                                />
                              </Button>
                              <Button
                                sx={{
                                  color: "red",
                                  "&:hover": {
                                    color: "red",
                                  },
                                }}
                              >
                                <AiOutlineDelete
                                  style={{
                                    width: "1.22rem",
                                    height: "1.22rem",
                                  }}
                                  onClick={() => deleteChapter("id")}
                                />
                              </Button>
                            </Box>
                          </Box>
                        );
                      })}
                    </Box>
                  </Box>
                </div>
              </Box>
              <button
                type="submit"
                class="inline-flex items-center px-5 py-2.5 sm:mt-6 text-sm font-medium text-center text-white bg-indigo-700 rounded-md focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800"
                style={{
                  backgroundColor: "#754ffe",
                }}
              >
                Save
              </button>
            </form>
          </div>
        </section>
      </Box>
    </>
  );
}
