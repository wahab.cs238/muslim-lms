import React from "react";
import { pageCss } from "../page/PageCss";
import { Box, Stack, Typography } from "@mui/material";
import imgblog from "../image/course-react.jpg";
import { Link } from "react-router-dom";

const ArticleCard = () => {
  const classes = pageCss();
  return (
    <>
      <Box className={classes.blog_section_box}>
        <Box className={classes.blog_section_box_thumbnail}>
          <Link to="/article-details">
            <img src={imgblog} alt="img" className={classes.blog_thumbnail} />
          </Link>
        </Box>
        <Typography
          variant="h4"
          component="h4"
          className={classes.blog_category}
        >
          Category here
        </Typography>
        <Typography variant="h2" component="h2" className={classes.blog_title}>
          <Link to="/article-details">
            What is machine learning and how does it work?
          </Link>
        </Typography>
        <Typography variant="h4" component="p" className={classes.blog_des}>
          Lorem ipsum dolor sit amet, accu msan in, tempor dictum nequrem
          ipsum...
        </Typography>
        <Link to="/">
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              px: "1.1rem",
              pb: "1rem",
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <img
                style={{ height: "2rem", width: "2rem" }}
                class="object-cover rounded-full"
                src="https://images.unsplash.com/photo-1586287011575-a23134f797f9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=48&q=60"
                alt="Avatar"
              />
              <Stack sx={{ ml: ".6rem", alignItems: "start" }}>
                <Typography
                  variant="body1"
                  sx={{
                    fontSize: ".8rem",
                    fontWeight: "bold",
                    margin: 0,
                    color: "#754ffe",
                    "&:hover": {
                      color: "#a190e2",
                    },
                  }}
                >
                  Jone Doe
                </Typography>
                {/* <span
                style={{
                  fontSize: ".7rem",
                  fontWeight: "bold",
                  margin: 0,
                  color: "gray",
                }}
              >
                21 SEP 2015
              </span> */}
              </Stack>
            </Box>
          </Box>
        </Link>
      </Box>
      {/* <div class="flex flex-col justify-center items-center bg-gray-100 min-h-screen">
        <div class="bg-white rounded-lg shadow-lg overflow-hidden max-w-lg w-full">
          <img
            src="https://images.unsplash.com/photo-1454496522488-7a8e488e8606"
            alt="Mountain"
            class="w-full h-64 object-cover"
          />
          <div class="p-6">
            <h2 class="text-2xl font-bold text-gray-800 mb-2">
              Beautiful Mountain View
            </h2>
            <p class="text-gray-700 leading-tight mb-4">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
              eu sapien porttitor, blandit velit ac, vehicula elit. Nunc et ex
              at turpis rutrum viverra.
            </p>
          
          </div>
        </div>
      </div> */}
    </>
  );
};

export default ArticleCard;
