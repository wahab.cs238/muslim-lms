import { Avatar, Box, Stack, Typography } from "@mui/material";
import React from "react";
import { NavLink } from "react-router-dom";

import StarIcon from "@mui/icons-material/Star";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import GroupIcon from "@mui/icons-material/Group";
import PlayCircleIcon from "@mui/icons-material/PlayCircle";
import ShowMoreText from "react-show-more-text";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";

import compactFormat from "cldr-compact-number";

const InstructCourseDetails = ({ instructor, averageRating, totalViews }) => {
  return (
    <Box
      sx={{
        marginTop: "5rem",
        marginBottom: "7rem",
      }}
    >
      <Typography
        variant="h4"
        sx={{
          fontWeight: 700,
          mt: { xs: 0, sm: "4rem" },
          fontSize: { xs: "1.4rem", sm: "2rem" },
        }}
      >
        Instructor
      </Typography>
      <Box>
        <Typography
          component={NavLink}
          to={"/user/wer"}
          sx={{
            fontWeight: 700,
            color: "#7543fe",
            textDecoration: "underline",
            "&:hover": {
              color: "#3300bf",
            },
            textTransform: "capitalize",
          }}
        >
          {instructor?.name}
        </Typography>
        <Typography>'bio'</Typography>
      </Box>
      {/* profile image and icons section */}
      <Box
        sx={{
          mt: "1rem",
          display: "flex",
          gap: { xs: "1rem", sm: "2rem" },
        }}
      >
        <Avatar
          alt="Remy Sharp"
          src="https://scontent.fkdh2-1.fna.fbcdn.net/v/t39.30808-6/233207203_369928588181620_6943518076439425308_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=5f2048&_nc_ohc=Fm3rvdZvQ7UAX-QKKZd&_nc_ht=scontent.fkdh2-1.fna&oh=00_AfALTfCBVMozCr5OdHQDt-oj3JYa1WAAzkHky5D-Yu2qLA&oe=660D920F"
          sx={{ width: 100, height: 100 }}
        />
        <Stack gap=".3rem">
          <Box
            sx={{
              display: "flex",
              gap: ".7rem",
              alignItems: "center",
            }}
          >
            <StarIcon
              style={{
                fontSize: "1rem",
              }}
            />
            <Typography
              sx={{
                fontSize: ".8rem",
                opacity: 0.8,
                fontFamily: "sans-serif !important",
              }}
            >
              {averageRating} Average Ratings
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              gap: ".7rem",
              alignItems: "center",
            }}
          >
            <RemoveRedEyeIcon
              style={{
                fontSize: "1rem",
              }}
            />
            <Typography
              sx={{
                fontSize: ".8rem",
                opacity: 0.8,
                fontFamily: "sans-serif !important",
              }}
            >
              {
                compactFormat(totalViews, "en", {
                  significantDigits: 1,
                  minimumFractionDigits: 1,
                  maximumFractionDigits: 2,
                })
                // 19.6K
              }{" "}
              views
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              gap: ".7rem",
              alignItems: "center",
            }}
          >
            <CloudDownloadIcon
              style={{
                fontSize: "1rem",
              }}
            />
            <Typography
              sx={{
                fontSize: ".8rem",
                fontFamily: "sans-serif !important",
                opacity: 0.8,
              }}
            >
              {
                compactFormat('23423423', "en", {
                  significantDigits: 1,
                  minimumFractionDigits: 1,
                  maximumFractionDigits: 2,
                })
                // 19.6K
              }{" "}
              Downloads
            </Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              gap: ".7rem",
              alignItems: "center",
            }}
          >
            <PlayCircleIcon
              style={{
                fontSize: "1rem",
              }}
            />
            <Typography
              sx={{
                fontSize: ".8rem",
                opacity: 0.8,
                fontFamily: "sans-serif !important",
              }}
            >
              {instructor?.courses?.length >= 1
                ? `${instructor?.courses?.length} Courses`
                : `${instructor?.courses?.length} Course`}
            </Typography>
          </Box>
        </Stack>
      </Box>
      {/* profile image and icons section  end*/}

      {/* Description start */}
      <Box
        sx={{
          mt: "1.5rem",
        }}
      >
        <ShowMoreText
          /* Default options */
          lines={3}
          more="Show more"
          less="Show less"
          className="content-css"
          anchorClass="show-more-less-clickable"
          // onClick={this.executeOnClick}
          expanded={false}
          truncatedEndingComponent={"... "}
        >
          Bundling the courses and know how of successful instructors, Academind
          strives to deliver high quality online education. Online Education,
          Real-Life Success - that's what Academind stands for. Learn topics
          like web development, data analyses and more in a fun and engaging
          way. We've taught more than 2,000,000 students on a broad variety of
          topics. We'd love to GBundling the courses and know how of successful
          instructors, Academind strives to deliver high quality online
          education. Online Education, Real-Life Success - that's what Academind
          stands for. Learn topics like web development, data analyses and more
          in a fun and engaging way. We've taught more than 2,000,000 students
          on a broad variety of topics. We'd love to
        </ShowMoreText>
      </Box>
      {/* Description end */}
    </Box>
  );
};

export default InstructCourseDetails;
