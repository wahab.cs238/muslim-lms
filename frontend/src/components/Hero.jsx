import { Box, Button, Container, Typography } from "@mui/material";
import React from "react";
import { comCss } from "./ComponentsCss";
import heroimg from "../image/hero-img.png";
import { NavLink } from "react-router-dom";
import Lottie from "react-lottie";
import animationData from "../lotties/Animation1.json";
import useMediaQuery from "@mui/material/useMediaQuery";
import { Swiper, SwiperSlide } from "swiper/react";
import animationData1 from "../lotties/ویډیو ګانی سره تیریدل.json";

//
import "swiper/css";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const defaultOptions1 = {
  loop: true,
  autoplay: true,
  animationData: animationData1,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

//

const Hero = () => {
  const comclasses = comCss();
  const classes = comCss();
  const matches_450 = useMediaQuery("(max-width:450px)");

  return (
    <Swiper className="mySwiper">
      <SwiperSlide>
        <Box className={classes.hero_section}>
          <Container maxWidth="lg">
            <Box className={classes.hero_banner}>
              <Box className={classes.hero_banner_laft}>
                <Typography
                  variant="h2"
                  component="h2"
                  className={classes.hero_title}
                >
                  Welcome to{" "}
                  <span style={{ color: " rgb(0 255 177)" }}>
                    Muslim Afghan Learning System
                  </span>
                </Typography>

                <Typography
                  variant="h5"
                  component="p"
                  className={classes.hero_des}
                >
                  Hand-picked Instructor and expertly crafted courses, designed
                  for the modern students and entrepreneur.
                </Typography>
                <Box className={classes.hero_button}>
                  <Button
                    sx={{ marginRight: "12px" }}
                    component={NavLink}
                    to="/courses"
                    className={`${classes.button} ${classes.button_3}`}
                  >
                    Search Courses
                  </Button>
                  {/* <Button
                    ccomponent={NavLink}
                    to="/"
                    className={`${classes.button} ${classes.button_1}`}
                  >
                    Are You Instructor?
                  </Button> */}
                </Box>
              </Box>
              <Box className={classes.hero_banner_right}>
                <Lottie
                  options={defaultOptions}
                  height={matches_450 ? 300 : 400}
                  width={matches_450 ? 300 : 400}
                  speed={0.5}
                />
              </Box>
            </Box>
          </Container>
        </Box>
      </SwiperSlide>
      <SwiperSlide>
        <div
          className="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8"
          style={{ marginTop: "7rem", marginBottom: "6rem" }}
        >
          {/* <!-- Grid --> */}
          <div className="grid md:grid-cols-2 gap-4 md:gap-8 xl:gap-20 md:items-center">
            <div style={{ textAlign: "start" }}>
              <h1 className="block text-3xl font-bold text-gray-800 sm:text-4xl lg:text-5xl lg:leading-tight dark:text-white ">
                Find you'r favourite course with{" "}
                <span style={{ color: "#754ffe" }}>Muslim Learning System</span>
              </h1>
              <p className="mt-3 text-lg text-gray-800 dark:text-gray-400">
                Hand-picked professionals and expertly crafted components,
                designed for any kind of entrepreneur.
              </p>

              {/* <!-- Buttons --> */}
              <div className="mt-7 grid gap-3 w-full sm:inline-flex">
                <Button
                  sx={{
                    marginRight: "12px",
                    display: "flex",
                    flexDirection: "row",
                  }}
                  component={NavLink}
                  to="/courses"
                  className={`${comclasses.button} ${comclasses.button_4}`}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                      gap: "4px",
                      width: "10rem",
                    }}
                  >
                    Search Courses
                    <svg
                      className="flex-shrink-0 size-4"
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <path d="m9 18 6-6-6-6" />
                    </svg>
                  </Box>
                </Button>

                {/* <a className="py-3 px-4 inline-flex justify-center items-center gap-x-2 text-sm font-medium rounded-lg border border-gray-200 bg-white text-gray-800 shadow-sm hover:bg-gray-50 disabled:opacity-50 disabled:pointer-events-none dark:bg-slate-900 dark:border-gray-700 dark:text-white dark:hover:bg-gray-800 dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600" href="#">
                Contact sales team
              </a> */}
              </div>
              {/* <!-- End Buttons --> */}
            </div>
            {/* <!-- End Col --> */}

            <div className="relative ms-4">
              <Lottie
                options={defaultOptions1}
                height={matches_450 ? 250 : 400}
                width={matches_450 ? 250 : 400}
                speed={0.5}
              />
            </div>
            {/* <!-- End Col --> */}
          </div>
          {/* <!-- End Grid --> */}
        </div>
      </SwiperSlide>
    </Swiper>
  );
};

export default Hero;
