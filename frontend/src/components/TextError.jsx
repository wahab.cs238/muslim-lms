const TextError = (props) => {
  return (
    <h3
      style={{ fontSize: "1rem", fontWeight: "bold" }}
      className="text-red-600"
    >
      {props.error}
    </h3>
  );
};
export default TextError;
