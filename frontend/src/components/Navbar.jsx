import {
  Box,
  Button,
  Container,
  Drawer,
  Hidden,
  IconButton,
  InputBase,
} from "@mui/material";
import React, { useState } from "react";
import { comCss } from "./ComponentsCss";
import logo from "../image/logo.svg";
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";
import { NavLink } from "react-router-dom";

// languaage PopOver
import LanguagePopover from "./LanguagePopover";

// store
import { selectIsAdmin } from "../store/auth/user/userSlice";
import { useSelector } from "react-redux";

const Navbar = () => {
  // redux
  const isAdmin = useSelector(selectIsAdmin);
  const classes = comCss();
  const [openMenu, setOpenMenu] = useState(false);

  const [scrollNavbar, setScrollNavbar] = useState(false);
  const changeBackground = () => {
    if (window.scrollY >= 90) {
      setScrollNavbar(true);
    } else {
      setScrollNavbar(false);
    }
  };
  window.addEventListener("scroll", changeBackground);

  return (
    <Box
      className={classes.navbar_section_active}
      // : `${classes.navbar_section}`
      sx={{
        justifyContent: "space-between",
      }}
    >
      <Container maxWidth="lg" sx={{}}>
        <Box className={classes.navbar_box}>
          <Box className={classes.navbar_laft}>
            <Box className={classes.navbar_laft_logo}>
              <NavLink to="/">
                <img src={logo} alt="logo" className={classes.img_responsive} />
              </NavLink>
            </Box>
            <Box className={classes.navbar_laft_menu}>
              <Box className={classes.navbar_link_computer}>
                <NavLink to="/" className={`${classes.nav_link}`}>
                  Home
                </NavLink>

                <NavLink to="courses" className={`${classes.nav_link}`}>
                  Courses
                </NavLink>
                <NavLink to="articles" className={`${classes.nav_link}`}>
                  Articles
                </NavLink>
                <NavLink to="about" className={`${classes.nav_link}`}>
                  About
                </NavLink>
                <NavLink to="contact" className={`${classes.nav_link}`}>
                  Contact
                </NavLink>
                <NavLink to="/user/123123" className={`${classes.nav_link}`}>
                  profile
                </NavLink>
                <LanguagePopover />
              </Box>
              <Box className={classes.navbar_link_mobail}>
                <IconButton onClick={() => setOpenMenu(!openMenu)}>
                  <MenuIcon />
                </IconButton>
                <Drawer
                  anchor={"right"}
                  open={openMenu}
                  onClose={() => setOpenMenu(!openMenu)}
                  classes={{
                    paper: classes.drawerPaper,
                  }}
                >
                  <IconButton
                    onClick={() => setOpenMenu(!openMenu)}
                    className={classes.clossessideNav}
                  >
                    <CloseIcon />
                  </IconButton>

                  {isAdmin ? (
                    <Button
                      component={NavLink}
                      to={"/admin"}
                      variant="contained"
                      disableElevation
                      mb={"1rem"}
                      sx={{
                        backgroundColor: "#754ffe",
                        "&:hover": {
                          backgroundColor: "#754ffe",
                          color: "white",
                        },
                      }}
                    >
                      Dashboard
                    </Button>
                  ) : null}

                  <NavLink
                    style={{ marginTop: "1rem" }}
                    to="/"
                    className={`${classes.nav_link} ${classes.nav_link_mobail}`}
                  >
                    Home
                  </NavLink>

                  <NavLink
                    to="courses"
                    className={`${classes.nav_link} ${classes.nav_link_mobail}`}
                  >
                    Courses
                  </NavLink>
                  <NavLink
                    to="articles"
                    className={`${classes.nav_link} ${classes.nav_link_mobail}`}
                  >
                    Articles
                  </NavLink>
                  <NavLink
                    to="about"
                    className={`${classes.nav_link} ${classes.nav_link_mobail}`}
                  >
                    About
                  </NavLink>
                  <NavLink
                    to="contact"
                    className={`${classes.nav_link} ${classes.nav_link_mobail}`}
                  >
                    Contact
                  </NavLink>
                </Drawer>
              </Box>
            </Box>
          </Box>
          {/* button left nav bar */}
          <Box className={classes.navbar_right}>
            {/* <Button component={NavLink} to="/" sx={{ marginRight: "12px" }}>
              SIgn In
            </Button>
            <Button component={NavLink} to="/">
              SIgn Up
            </Button> */}
          </Box>
          <Hidden mdDown>
            {/* <input
              type="text"
              id="search-navbar"
              class="block p-2 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-200 focus:ring-blue-500 focus:border-blue-500  dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Search..."
            /> */}

            {isAdmin ? (
              <Button
                component={NavLink}
                to={"/admin"}
                variant="contained"
                disableElevation
                mb={"1rem"}
                sx={{
                  backgroundColor: "#754ffe",
                  "&:hover": {
                    backgroundColor: "#754ffe",
                    color: "white",
                  },
                }}
              >
                Dashboard
              </Button>
            ) : null}
          </Hidden>
        </Box>
      </Container>
    </Box>
  );
};
export default Navbar;
