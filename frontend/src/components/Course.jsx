import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import StarRating from "./StarRating.jsx";
import "../App.css";
import { FaStar, FaStarHalfAlt } from "react-icons/fa";
import { Box, Typography } from "@mui/material";
import InstructCourseDetails from "./InstructCourseDetails.jsx";
const starsStyle = {
  fontSize: ".8rem",
  color: "#b4690e",
};
const ratings = {
  stars: [
    <FaStar style={starsStyle} key={1} />,
    <FaStar style={starsStyle} key={2} />,
    <FaStar style={starsStyle} key={3} />,
    <FaStar style={starsStyle} key={4} />,
    <FaStarHalfAlt style={starsStyle} key={5} />,
  ],
  title: "4.5",
};

const Course = ({ course, width }) => {
  const [averageRating, setAverageRating] = useState(0);
  let countAverageRating = 0;

  const { id, thumbnail, title, instructor, totalReviews, reviews, category } =
    course;

  reviews?.forEach((review) => {
    countAverageRating += review?.rating;
    // console.log(review?.rating)
  });

  countAverageRating = (countAverageRating / reviews?.length).toFixed(1);

  return (
    <CourseCard>
      <div
        className="item-img"
        style={{ position: "relative", backgroundColor: "white" }}
      >
        <Typography
          sx={{
            position: "absolute",
            top: "7.5rem",
            right: "8px",
            fontSize: ".9rem",
            backgroundColor: "#754ffe",
            color: "white",
            px: ".3rem",
            borderRadius: "3px",
          }}
        >
          پـښتو
        </Typography>
        <img src={thumbnail} alt={title} />
      </div>
      <div className="item-body">
        <Typography
          style={{
            textAlign: "start",
            fontSize: ".9rem",
            fontWeight: 600,
          }}
        >
          {title?.slice(0, 50)}
        </Typography>
        <Typography
          component={"p"}
          sx={{
            fontSize: "12px",
            textAlign: "start",
            mt: "8px",
            textTransform: "capitalize",
          }}
        >
          {instructor?.name}
        </Typography>

        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: ".5rem",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              gap: "2px",
            }}
          >
            {ratings?.stars?.map((star, idx) => {
              return star;
            })}
          </Box>
          <Typography
            sx={{
              fontSize: ".9rem",
              fontWeight: "600",
              // color: starsStyle.color,
            }}
          >
            {countAverageRating === "NaN" ? "" : countAverageRating}
            <span
              style={{
                marginLeft: ".5rem",
              }}
            >
              ({totalReviews})
            </span>
          </Typography>
        </Box>
      </div>
      <div className="item-btns flex">
        <Link to={`/course-details/${id}`} className="item-btn see-details-btn">
          See details
        </Link>
        <Link to="" className="item-btn add-to-cart-btn">
          Add to Playlist
        </Link>
      </div>
    </CourseCard>
  );
};

const CourseCard = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: rgba(149, 157, 165, 0.3) 0px 8px 24px;
  display: flex;
  flex-direction: column;

  background-color: #f7f7f7;

  width: 290px;
  height: 21rem;
  @media screen and (max-width: 400px) {
    width: 280px;
  }

  .item-body {
    margin: 10px 0;
    padding: 4px 18px;

    .item-name {
      font-size: 14px;
      line-height: 1.4;
      font-weight: 600;
      text-align: start;
    }
    .item-creator {
      font-size: 12.5px;
      font-weight: 500;
      color: rgba(0, 0, 0, 0.6);
    }
    .rating-star-val {
      margin-bottom: 5px;
      font-size: 14px;
      font-weight: 800;
      color: #b4690e;
      margin-right: 6px;
    }
    .rating-count {
      font-size: 12.5px;
      margin-left: 3px;
      font-weight: 500;
      opacity: 0.8;
    }
    .item-price-new {
      font-weight: 700;
      font-size: 15px;
    }
    .item-price-old {
      opacity: 0.8;
      font-weight: 500;
      text-decoration: line-through;
      font-size: 15px;
      margin-left: 8px;
    }
  }

  .item-btns {
    justify-self: flex-start;
    padding: 4px 8px 20px 18px;
    margin-top: auto;
    .item-btn {
      font-size: 15px;
      display: inline-block;
      padding: 6px 16px;
      font-weight: 700;
      transition: var(--transition);
      white-space: nowrap;

      &.see-details-btn {
        background-color: transparent;
        border: 1px solid #754ffe;
        margin-right: 5px;
        color: #754ffe;
        font-size: 0.8rem;
        font-weight: 600;

        &:hover {
          background-color: #754ffe;
          color: var(--clr-white);
        }
      }

      &.add-to-cart-btn {
        background: #754ffe;
        color: var(--clr-white);
        border: 1px solid #754ffe;
        font-size: 0.8rem;
        font-weight: 600;

        &:hover {
          background-color: transparent;
          color: #754ffe;
        }
      }
    }
  }
`;

export default Course;
