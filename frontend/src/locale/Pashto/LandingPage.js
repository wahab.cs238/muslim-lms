const LandingPage = {
  "Welcome To Muslim Afhgan Learning System":
    "مسلم افغان ښوونیز  سایټ ته ښه راغلاست",
  "Hand picked up Instructor and Expertly Crafted Courses, Designed For The Modern Students and Entrepremeur": `تکړه استادان او غوره کورسونو سره چی منتخب او غوره شاګردانو لپاره ډیزاین او ترتیب شوی دی `,
  "Search Courses": "کورسونه سرچ کړی",
  "Courses For you": "منتخب کورسونه ستاسو لپاره",
  "Most Populer": "غوره کورسونه",
  Read: "لوستل",
  "Top Categories": "طبقه بندی شوی",
  "Our Happy Clients Say About Us": "زموږ د زده کوونکوو رایه",
  "Our Achievements": "زموږ لاسته راوړني",
  Teachers: "ښوونکي",
  Videos: "ویدیویی درسونه",
  "App Users": "زموږ فعاله زده کوونکی",
  Home: "کور",
  Courses: "کورسونه",
  Articles: "مقالي",
  Contact: "اړیکه",
  profile: "مشخصات",
  "View All": "ټول کتل",
};

export default LandingPage;
