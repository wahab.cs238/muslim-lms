const LandingPage = {
  "Welcome To Muslim Afhgan Learning System":
    "خوش امدید به سایت اموزشی مسلم افغان",
  "Hand picked up Instructor and Expertly Crafted Courses, Designed For The Modern Students and Entrepremeur": ` اموزش ها به نوع جدید و پیشرفته با استادان باتجربه و دورس معیاری برای شاګردان ما `,
  "Search Courses": "اموزش ها را بیابید",
  "Courses For you": "اموزش ها برای شما",
  "Most Populer": "زیاد دیده شده",
  Read: "خواندن",
  "Top Categories": "طبقه های اموزشی",
  "Our Happy Clients Say About Us": "نظریات دانش اموزان ما",
  "Our Achievements": "دست اورد ها",
  Teachers: "معلم ها",
  Videos: "ویدیو های اموزشی",
  "App Users": "کسان فعال",
  Home: "خانه",
  Courses: "اموزش ها",
  Articles: "مقاله ها",
  Contact: "تماس با ما",
  profile: "مشخصات",
};

export default LandingPage;
