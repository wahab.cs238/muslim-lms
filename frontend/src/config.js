export const defaultSettings = {
  themeDirection: "ltr",
  themeLayout: "horizontal",
};

// @mui
import { faIR, ptPT, enUS } from "@mui/material/locale";

export const allLangs = [
  {
    label: "پښتو",
    value: "ps",
    systemValue: ptPT,
    // icon: AFG,
  },
  {
    label: "دری",
    value: "fa",
    systemValue: faIR,
    // icon: AFG,
  },
  {
    label: "English",
    value: "en",
    systemValue: enUS,
    // icon: USA,
  },
];

export const defaultLang = allLangs[0]; // Pashto
