import { useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Home from "./page/Home";
import About from "./page/About";
import Contact from "./page/Contact";
import CourseDetails from "./page/CourseDetails";
import Privacypolicy from "./page/Privacypolicy";
import Faq from "./page/Faq";
import ScrollToTops from "./components/ScrollToTops";
import ProductsFilters from "./components/ProductsFilters";
import CustomerProfile from "./components/Users/Profile/CustomerProfile";
import AdminDashboard from "./components/Admin/AdminDashboard";
import UpdateCategory from "./components/Admin/Categories/UpdateCategory";
import AddCategory from "./components/Admin/Categories/AddCategory";
import ManageCategories from "./components/Admin/Categories/ManageCategories";
import AddBrand from "./components/Admin/Categories/AddBrand";
import BrandsColorsList from "./components/Admin/Categories/BrandsColorsList";
import AddColor from "./components/Admin/Categories/AddColor";

import Articles from "./page/Articles";
import ArticleDetails from "./page/ArticleDetails";
import InstructorProfile from "./page/InstructorProfile";
import ManageCourses from "./components/Admin/Courses/ManageCourses";
import Section from "./components/Admin/Courses/Section";
import SectionPreview from "./components/Admin/Courses/SectionPreview";
import AddCourse from "./components/Admin/Courses/AddCourse";
import ManageArticles from "./components/Admin/Articles/ManageArticles";
import Settings from "./components/Admin/Settings";
import AddArticle from "./components/Admin/Articles/AddArticle";
import AddChapter from "./components/Admin/Courses/AddChapter";
import EditChapter from "./components/Admin/Courses/EditChapter";
import AddVideoToPlayList from "./components/Admin/Courses/AddVideoToPlayList";
import EditCourse from "./components/Admin/Courses/EditCourse";
import EditArticle from "./components/Admin/Articles/EditArticle";
// auth pages
import Register from "./page/auth/Register";
import Login from "./page/auth/Login";
import Forgot from "./page/auth/Forgot";
import Reset from "./page/auth/Reset";

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import AddCourseStep from "./components/Admin/Courses/AddCourseStep";
import { Box } from "@mui/material";

import { getAllArticles } from "./store/features/article/articleSlice";
import { useDispatch } from "react-redux";
function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllArticles());
  }, []);
  const queryClient = new QueryClient();

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <Navbar />
        <Box sx={{ mt: "4rem" }} />
        <Routes>
          {/* auth */}
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/reset" element={<Reset />} />
          <Route path="/forgot" element={<Forgot />} />
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/user/:id" element={<InstructorProfile />} />
          <Route path="/courses" element={<ProductsFilters />} />
          <Route path="/course-details/:id" element={<CourseDetails />} />
          <Route path="/articles" element={<Articles />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/article-details" element={<ArticleDetails />} />
          <Route path="/privacypolicy" element={<Privacypolicy />} />
          <Route path="/profile" element={<CustomerProfile />} />
          <Route path="/faq" element={<Faq />} />
          {/*  */}
          <Route path="admin" element={<AdminDashboard />}>
            <Route path="settings" element={<Settings />} />
            {/* <Route path="add-course" element={<AddCourse />} /> */}
            <Route path="add-course" element={<AddCourseStep />} />
            <Route path="edit-course/:id" element={<EditCourse />} />
            <Route path="manage-courses" element={<ManageCourses />} />
            <Route path="section/:id" element={<Section />} />
            <Route
              path="section-preview/:section_pre_Id"
              element={<SectionPreview />}
            />
            {/* course chapters */}
            <Route path="add-chapter/:courseId" element={<AddChapter />} />
            <Route path="edit-chapter/:courseId" element={<EditChapter />} />
            <Route
              path="add-video/:sectionId"
              element={<AddVideoToPlayList />}
            />
            {/* articles */}
            <Route path="add-article" element={<AddArticle />} />
            <Route path="edit-article/:id" element={<EditArticle />} />
            <Route path="manage-articles" element={<ManageArticles />} />
            {/* category */}
            <Route path="add-category" element={<AddCategory />} />
            <Route path="manage-category" element={<ManageCategories />} />
            <Route path="edit-category/:id" element={<UpdateCategory />} />
            <Route path="add-brand" element={<AddBrand />} />
            <Route path="all-brands" element={<BrandsColorsList />} />
            <Route path="add-color" element={<AddColor />} />
            <Route path="all-colors" element={<BrandsColorsList />} />
          </Route>
        </Routes>
        <ScrollToTops />
      </QueryClientProvider>
    </>
  );
}

export default App;
