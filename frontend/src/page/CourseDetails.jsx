import { Box, Container, Grid, Typography, Rating } from "@mui/material";
import React, { useEffect, useState } from "react";
import Pagebanner from "../components/Pagebanner";
import { pageCss } from "./PageCss";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import CourseSidebar from "../components/CourseSideBar";
import { PlayArrow } from "@mui/icons-material";
import { Link, useParams } from "react-router-dom";
import InstructCourseDetails from "../components/InstructCourseDetails";
import Footer from "../components/Footer";
import courses from "../utils/data";
import Course from "../components/Course";
import FetchFromApi from "../utils/FetchFromApi";
import axios from "axios";
import SuccessMsg from "../components/SuccessMsg/SuccessMsg";

const itemtabs = [
  "Introduction",
  "Understanding React Native Fundamentals",
  "Adding Stack and Bottom Tab Navigator to our react native project",
  "Overview of React Hooks",
  "Project",
  "Conclusion",
];
const tabData = [1, 2, 3, 4];

const CourseDetails = () => {
  const classes = pageCss();
  const [tabValue, setTabvalue] = useState(0);
  const [open, setOpen] = useState(null);
  const [data, setData] = useState([]);
  const [instructor, setInstructor] = useState([]);
  const [review, setReview] = useState({
    comment: "",
    rating: "",
  });
  const [message, setMessage] = useState({
    error: "",
    success: "",
    loading: false,
  });

  const { id } = useParams();

  const handleChange = (event, newValue) => {
    setTabvalue(newValue);
  };

  const handleChangeOpen = (value) => {
    if (value === open) {
      return setOpen(null);
    }
    setOpen(value);
  };

  useEffect(() => {
    FetchFromApi(`courses/${id}`).then((data) => setData(data));
  }, []);

  let countAverageRating = 0;

  data?.course?.reviews?.forEach((review) => {
    countAverageRating += review?.rating;
    // console.log(review?.rating)
  });

  countAverageRating = (
    countAverageRating / data?.course?.reviews?.length
  ).toFixed(1);

  // review handler

  const handleChangeReview = (e) => {
    const { name, value } = e.target;
    setReview({ ...review, [name]: value });
  };
  console.log(data);

  // review submit handler

  const handleSubmitReview = async (e) => {
    const response = await axios.post(
      `http://localhost:3000/reviews/${data?.course?.id}`,
      {
        rating: review?.rating,
        comment: review?.comment,
      },
      {
        headers: {
          Authorization: token,

          ContentType: "application/json",
        },
      }
    );

    if (response?.data?.error) {
      setMessage({ ...message, error: response?.data?.error });
    } else if (response?.data) {
    }
    console.log("this is response : ", response);
  };

  return (
    <>
      {message?.error && (
        <SuccessMsg title={"Failed"} icon={"error"} message={message?.error} />
      )}

      <Pagebanner
        title={data?.course?.title}
        subtitle={data?.course?.description}
        course_time="12 hours 34 minutes"
        course_enroll={data?.totalViews}
        course_rating={countAverageRating}
      />
      <Box className={classes.course_banner}></Box>
      <Container maxWidth="lg">
        <Grid
          container
          spacing={4}
          sx={{
            display: "flex",
            flexDirection: {
              xs: "column-reverse",
              sm: "column-reverse",
              md: "row",
            },
          }}
        >
          <Grid item xs={12} sm={12} md={8}>
            <Typography
              variant="h4"
              sx={{
                fontWeight: 700,
                mt: { xs: "1rem", sm: "4rem" },
                fontSize: { xs: "1.4rem", sm: "2rem" },
                mb: { xs: "3rem", sm: 0 },
              }}
            >
              Course Content
            </Typography>
            <Box
              className={classes.single_course_tabs_section}
              sx={{ mt: "3rem" }}
            >
              {/* <Tabs value={tabValue} onChange={handleChange}>
                <Tab label="Lectures" />
                <Tab label="Description" />


                


              </Tabs> */}

              <Box className={classes.single_course_tabs_box}>
                {tabValue === 0 && (
                  <Box className={classes.single_course_tabs}>
                    {data?.course?.sections?.map((item, idx) => (
                      <div id="accordion-collapse" data-accordion="collapse">
                        <h2
                          id="accordion-collapse-heading-1"
                          className="border-b"
                        >
                          <button
                            type="button"
                            class="flex items-center justify-between w-full p-4 font-medium rtl:text-right text-gray-500 border border-b-0 border-gray-200 rounded-t-xl focus:ring-4 focus:ring-gray-200  dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800 gap-3 "
                            data-accordion-target="#accordion-collapse-body-1"
                            aria-expanded="true"
                            aria-controls="accordion-collapse-body-1"
                            onClick={() => handleChangeOpen(idx)}
                          >
                            <span
                              style={{
                                fontSize: "1.1rem",
                                color: "black",
                                textAlign: "start",
                              }}
                            >
                              {item?.title}
                            </span>
                            <svg
                              data-accordion-icon
                              class="w-3 h-3 rotate-180 shrink-0"
                              aria-hidden="true"
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 10 6"
                            >
                              <path
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M9 5 5 1 1 5"
                              />
                            </svg>
                          </button>
                        </h2>
                        <div
                          id="accordion-collapse-body-1"
                          class={open === idx ? "" : "hidden"}
                          aria-labelledby="accordion-collapse-heading-1"
                        >
                          <div class="p-5 border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p
                              class="mb-2 text-gray-600 dark:text-gray-400"
                              style={{
                                fontSize: "1rem",
                                color: "black",
                              }}
                            >
                              {item?.videos?.map((vdo) => (
                                <Box
                                  key={vdo}
                                  className={classes.single_course_tabs_content}
                                >
                                  <Typography
                                    variant="h3"
                                    sx={{
                                      textDecoration: "underline",
                                      color: "#411cffb5",
                                      cursor: "pointer",
                                      textAlign: { xs: "center", sm: "start" },
                                      fontSize: {
                                        xs: "12px !important",
                                        sm: "0.9rem !important",
                                      },
                                      fontWeight: 600,
                                    }}
                                  >
                                    {/* play arrow */}
                                    <PlayArrow
                                      className={
                                        classes.single_course_tabs_icon
                                      }
                                    />
                                    {vdo?.video?.title}
                                  </Typography>

                                  <Typography variant="h4">
                                    {" "}
                                    {vdo?.video?.duration}
                                  </Typography>
                                </Box>
                              ))}
                            </p>
                          </div>
                        </div>
                      </div>
                    ))}
                  </Box>
                )}
                {tabValue === 1 && (
                  <Box className={classes.single_course_tabs_des}>
                    <Typography
                      sx={{
                        textAlign: { xs: "center", sm: "start" },
                        fontSize: {
                          xs: "1rem !important",
                          sm: "1rem !important",
                        },
                      }}
                    >
                      If you’re learning to program for the first time, or if
                      you’re coming from a different language, this course,
                      JavaScript: Getting Started, will give you the basics for
                      coding in JavaScript. First, you’ll discover the types of
                      applications that can be built with JavaScript, and the
                      platforms they’ll run on.
                    </Typography>
                    <Typography
                      sx={{
                        pt: "1rem",
                        textAlign: { xs: "center", sm: "start" },
                        fontSize: {
                          xs: "1rem !important",
                          sm: "1rem !important",
                        },
                      }}
                    >
                      Next, you’ll explore the basics of the language, giving
                      plenty of examples. Lastly, you’ll put your JavaScript
                      knowledge to work and modify a modern, responsive web
                      page. When you’re finished with this course, you’ll have
                      the skills and knowledge in JavaScript to create simple
                      programs, create simple web applications, and modify web
                      pages.
                    </Typography>
                  </Box>
                )}
              </Box>
            </Box>
          </Grid>
          {/* course details sidebar */}
          <Grid
            item
            xs={12}
            sm={12}
            md={4}
            mb={{ xs: "0", sm: "10rem" }}
            mt={{ xs: 0, sm: "0", md: "5rem" }}
          >
            <CourseSidebar />
          </Grid>
          {/* course details end */}
        </Grid>
        <Box
          sx={{
            px: { xs: 0, sm: "24px" },
            mt: { xs: "3rem", sm: null },
          }}
        >
          <Typography
            variant="h4"
            sx={{
              fontWeight: 700,
              mt: { xs: 0, sm: "0" },
              fontSize: { xs: "1.4rem", sm: "2rem" },
            }}
          >
            Description
          </Typography>
          <Typography
            variant="h6"
            sx={{
              fontSize: { xs: ".8rem", sm: "1rem" },
              mt: "1rem",
              letterSpacing: "1px",
            }}
          >
            {data?.course?.description}
          </Typography>
          {/* instructor profile */}
          <InstructCourseDetails
            instructor={data?.course?.instructor}
            averageRating={data?.averageRating}
            totalViews={data?.totalViews}
          />
          {/* instructor profile */}
        </Box>
        {/* add reviews to course */}

        <section
          class="bg-white dark:bg-gray-900 antialiased"
          style={{ width: "100%" }}
        >
          <div class="mx-auto ">
            <div class="flex justify-between items-center mb-6">
              <Typography
                variant="h4"
                sx={{
                  fontWeight: 700,
                  mt: { xs: 0, sm: "0" },
                  fontSize: { xs: "1.4rem", sm: "2rem" },
                  textAlign: { xs: "center", sm: "start" },
                  width: "100%",
                }}
              >
                {" "}
                {data?.course?.reviews.length === 0
                  ? "Be the first to review this course"
                  : `Course Reviews & Ratings (${data?.course?.reviews?.length})`}
              </Typography>
            </div>
            {/* give rating */}
            <p class=" items-center text-sm text-gray-900 dark:text-white font-semibold">
              Select Rating
            </p>
            <Rating
              name="rating"
              value={review?.rating}
              onChange={handleChangeReview}
              sx={{ mb: "1.2rem", mt: "1rem" }}
            />
            <div class="py-2 px-4 mb-4 bg-white rounded-lg rounded-t-lg border border-gray-200 dark:bg-gray-800 dark:border-gray-700">
              <label for="comment" class="sr-only">
                Your comment
              </label>
              {/* give comment */}
              <textarea
                id="comment"
                rows="6"
                name="comment"
                onChange={handleChangeReview}
                class="px-0 w-full text-sm text-gray-900 border-0 focus:ring-0 focus:outline-none dark:text-white dark:placeholder-gray-400 dark:bg-gray-800"
                placeholder="Write a comment..."
                required
              ></textarea>
            </div>
            <button
              onClick={handleSubmitReview}
              style={{
                backgroundColor: "#754ffe",
              }}
              className="inline-flex items-center py-2.5 px-4 text-xs font-medium text-center text-white rounded-md focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800"
            >
              Post Review
            </button>
          </div>
        </section>

        {/* add reviews to course end */}

        {/* course rating and reivews */}

        {data?.course?.reviews?.length === 0 ? null : (
          <section>
            <div className="mx-auto max-w-7xl ">
              <div className="flex flex-col items-center">
                <div className="text-center md:mt-16 md:order-3">
                  {/* <a
                  href="#"
                  title=""
                  className="pb-2 text-base font-bold leading-7 text-gray-900 transition-all duration-200 border-b-2 border-gray-900 hover:border-gray-600 font-pj focus:outline-none focus:ring-1 focus:ring-gray-900 focus:ring-offset-2 hover:text-gray-600"
                >
                  Check all 2,157 reviews
                </a> */}
                </div>

                <div className="relative mt-10 md:mt-24 md:order-2">
                  <div className="absolute -inset-x-1 inset-y-16 md:-inset-x-2 md:-inset-y-6">
                    <div
                      className="w-full h-full max-w-5xl mx-auto rounded-3xl opacity-30 blur-lg filter"
                      style={{
                        background:
                          "linear-gradient(90deg, #44ff9a -0.55%, #44b0ff 22.86%, #8b44ff 48.36%, #ff6644 73.33%, #ebff70 99.34%)",
                      }}
                    ></div>
                  </div>

                  <div className="relative grid max-w-lg grid-cols-1 gap-6 mx-auto md:max-w-none lg:gap-10 md:grid-cols-3">
                    {data?.course?.reviews?.map((review) => {
                      return (
                        <div className="flex flex-col overflow-hidden shadow-xl border-indigo-500 border-2 rounded-md">
                          <div
                            className="flex flex-col justify-between flex-1 p-6 bg-white lg:py-8 lg:px-7"
                            style={{
                              minWidth: "293px",
                              minHeight: "295px",
                            }}
                          >
                            <div className="flex-1">
                              <div className="flex items-center">
                                {[1, 1, 1, 1, 1].map((item, idx) => {
                                  if (idx === review?.rating) {
                                    return;
                                  }
                                  return (
                                    <svg
                                      className="w-5 h-5 text-[#FDB241]"
                                      xmlns="http://www.w3.org/2000/svg"
                                      viewBox="0 0 20 20"
                                      fill="currentColor"
                                    >
                                      <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                    </svg>
                                  );
                                })}
                              </div>

                              <blockquote className="flex-1 mt-8">
                                <p className="text-lg leading-relaxed text-gray-900 font-pj">
                                  {review?.comment}
                                </p>
                              </blockquote>
                            </div>

                            <div className="flex items-center mt-8">
                              <img
                                className="flex-shrink-0 object-cover rounded-full w-11 h-11"
                                src="https://cdn.rareblocks.xyz/collection/clarity/images/testimonial/4/avatar-male-2.png"
                                alt=""
                              />
                              <div className="ml-4">
                                <p className="text-base font-bold text-gray-900 font-pj">
                                  {review?.user?.name}
                                </p>
                                <p className="mt-0.5 text-sm font-pj text-gray-600">
                                  'bio here'
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </section>
        )}

        {/* course rating and reivews end */}

        {/* related Courses */}

        {data?.newRelatedCourses?.length === 0 ? null : (
          <Box
            sx={{
              px: { xs: 0, sm: "24px" },
              mt: { xs: "3rem", sm: "6rem" },
            }}
          >
            <Typography
              variant="h4"
              sx={{
                fontWeight: 700,
                mt: { xs: 0, sm: "0" },
                fontSize: { xs: "1.4rem", sm: "2rem" },
              }}
            >
              Related Courses
            </Typography>

            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: { xs: "center", sm: "start" },
                gap: "2rem",
                mt: "3rem",
                mb: "3rem",
              }}
            >
              {data?.newRelatedCourses?.map((item, idx) => {
                if (idx >= 5) {
                  return;
                }
                return <Course course={item} key={idx} />;
              })}
            </Box>
          </Box>
        )}

        {/* related Courses end */}

        {/* footer */}
        <Footer />
        {/* footer */}
      </Container>
    </>
  );
};

export default CourseDetails;
