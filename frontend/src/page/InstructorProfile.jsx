import { Avatar, Hidden, Stack } from "@mui/material";
import React, { useState } from "react";
import courses from "../utils/data";
import Course from "../components/Course";
import Footer from "../components/Footer";

import styled from "styled-components";
import { Link } from "react-router-dom";
import StarRating from "../components/StarRating.jsx";
import "../App.css";
import { FaStar, FaStarHalfAlt } from "react-icons/fa";

import { Box, Typography } from "@mui/material";

const starsStyle = {
  fontSize: ".8rem",
  color: "#b4690e",
};
const ratings = {
  stars: [
    <FaStar style={starsStyle} />,
    <FaStar style={starsStyle} />,
    <FaStar style={starsStyle} />,
    <FaStar style={starsStyle} />,
    <FaStarHalfAlt style={starsStyle} />,
  ],
  title: "4.5",
};

const InstructorProfile = () => {
  const [course, setCourse] = useState({
    id: 1,
    image: "asdfasdf",
    course_name: "asdfasdf",
    creator: "asdfasdf",
  });

  const { image, course_name, creator, id } = course;

  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          backgroundColor: "#f5f4f88c !important",
        }}
      >
        {/* container for two rows */}
        <Box
          sx={{
            mt: { xs: 0, sm: "4rem" },

            maxWidth: "1024px",
            display: "flex",
            flexDirection: { xs: "column-reverse", sm: "row" },
            gap: "3rem",
          }}
        >
          {/* row 1, name and other details */}
          <Box
            sx={{
              marginBottom: "7rem",
              px: { xs: "20px", sm: "32px" },
            }}
          >
            <Typography
              variant="h4"
              sx={{
                fontWeight: 700,

                fontSize: "1.4rem",
                color: "gray",
              }}
            >
              Instructor
            </Typography>
            <Box>
              <Typography
                sx={{
                  fontWeight: 700,
                  fontSize: { xs: "2rem", sm: "3rem" },
                }}
              >
                Raqib Yoon
              </Typography>
              <Typography
                sx={{
                  fontWeight: "400",
                  fontSize: "1.2rem",
                }}
              >
                Frontend web Developer
              </Typography>

              <Box
                sx={{
                  display: "flex",
                  gap: "3rem",
                  mt: "2rem",
                }}
              >
                <Stack>
                  <Typography
                    variant="h4"
                    sx={{
                      fontWeight: 700,
                      fontSize: "1rem",
                      color: "gray",
                    }}
                  >
                    Total Courses
                  </Typography>{" "}
                  <Typography
                    sx={{
                      fontWeight: "600",
                      fontSize: "1.2rem",
                    }}
                  >
                    10k
                  </Typography>
                </Stack>
                <Stack>
                  <Typography
                    variant="h4"
                    sx={{
                      fontWeight: 700,
                      fontSize: "1rem",
                      color: "gray",
                    }}
                  >
                    Reviews
                  </Typography>{" "}
                  <Typography
                    sx={{
                      fontWeight: "600",
                      fontSize: "1.2rem",
                    }}
                  >
                    10k
                  </Typography>
                </Stack>
              </Box>
            </Box>

            {/* About me */}
            <Box
              sx={{
                mt: "1.5rem",
              }}
            >
              <Typography
                variant="h4"
                sx={{
                  fontWeight: 700,
                  mt: "2rem",
                  mb: "1rem",
                  color: "#754ffe",
                  fontSize: "1.4rem",
                }}
              >
                About Me
              </Typography>
              <Typography
                variant="body1"
                sx={{
                  maxWidth: { xs: "auto", sm: "43vw" },
                }}
              >
                Bundling the courses and know how of successful instructors,
                Academind strives to deliver high quality online education.
                Online Education, Real-Life Success - that's what Academind
                stands for. Learn topics like web development, data analyses and
                more in a fun and engaging way. We've taught more than 2,000,000
                students on a broad variety of topics. We'd love to GBundling
                the courses and know how of successful instructors, Academind
                strives to deliver high quality online education. Online
                Education, Real-Life Success - that's what Academind stands for.
                Learn topics like web development, data analyses and more in a
                fun and engaging way. We've taught more than 2,000,000 students
                on a broad variety of topics. We'd love to
              </Typography>
            </Box>
            {/* about me end */}
            <Typography
              variant="h4"
              sx={{
                fontWeight: 700,
                mt: "3rem",
                mb: "2rem",
                fontSize: "1.4rem",
              }}
            >
              PlayList ({courses.length})
            </Typography>

            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: { xs: "center", sm: "start" },
                gap: "2rem",
              }}
            >
              {courses.map((item, idx) => {
                if (idx >= 5) {
                  return;
                }
                return (
                  <CourseCard>
                    <div
                      className="item-img"
                      style={{ position: "relative", backgroundColor: "white" }}
                    >
                      <Typography
                        sx={{
                          position: "absolute",
                          top: "7.5rem",
                          right: "8px",
                          fontSize: ".9rem",
                          backgroundColor: "#754ffe",
                          color: "white",
                          px: ".3rem",
                          borderRadius: "3px",
                        }}
                      >
                        پـښتو
                      </Typography>
                      <img src={image} alt={course_name} />
                    </div>
                    <div className="item-body">
                      <Typography
                        style={{
                          textAlign: "start",
                          fontSize: ".9rem",
                          fontWeight: 600,
                        }}
                      >
                        {course_name?.slice(0, 50)}
                      </Typography>
                      <Typography
                        component={"p"}
                        sx={{
                          fontSize: "12px",
                          textAlign: "start",
                          mt: "8px",
                        }}
                      >
                        {creator}
                      </Typography>

                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          gap: ".5rem",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            alignItems: "center",
                            gap: "2px",
                          }}
                        >
                          {ratings?.stars?.map((star) => {
                            return star;
                          })}
                        </Box>
                        <Typography
                          sx={{
                            fontSize: ".9rem",
                            fontWeight: "600",
                            color: starsStyle.color,
                          }}
                        >
                          {ratings.title}
                          <span
                            style={{
                              marginLeft: ".5rem",
                            }}
                          >
                            (2342)
                          </span>
                        </Typography>
                      </Box>
                    </div>
                    <div className="item-btns flex">
                      <Link
                        to={`/course-details/${id}`}
                        className="item-btn see-details-btn"
                      >
                        See details
                      </Link>
                      <Link to="" className="item-btn add-to-cart-btn">
                        Delete From Playlist
                      </Link>
                    </div>
                  </CourseCard>
                );
              })}
            </Box>

            <Hidden smUp>
              <Stack
                sx={{
                  justifyContent: "center",
                  mt: "3rem",
                  gap: ".5rem",
                }}
              >
                {[1, 1, 1].map(() => {
                  return (
                    <Link to={"/"} target="_blank">
                      <Box
                        sx={{
                          display: "flex",
                          px: "3rem",
                          border: "1px solid #754ffe",
                          height: "3.2rem",
                          justifyContent: "center",
                          alignItems: "center",
                          gap: ".4rem",
                          "&:hover": {
                            backgroundColor: "#edededbd",
                          },
                        }}
                      >
                        <Typography
                          sx={{
                            textTransform: "capitalize",
                            fontWeight: "500",
                          }}
                        >
                          twitter
                        </Typography>
                      </Box>
                    </Link>
                  );
                })}
              </Stack>
            </Hidden>
          </Box>

          {/* row 2, image and links */}

          <Box
            sx={{
              px: { xs: "20px", sm: "32px" },
              marginTop: { xs: "3rem", sm: "0" },
            }}
          >
            <Avatar
              alt="Remy Sharp"
              src="https://scontent.fkdh2-1.fna.fbcdn.net/v/t39.30808-1/419242635_2102495056770043_997954213065734750_n.jpg?stp=dst-jpg_s200x200&_nc_cat=107&ccb=1-7&_nc_sid=5f2048&_nc_ohc=TuWFwryZGdAAb7EdT-C&_nc_ht=scontent.fkdh2-1.fna&oh=00_AfAYeSEzKkcdfprbxysmwZcqfGwXTDFD2or7zOEoSzwh1A&oe=66217233"
              sx={{ width: 200, height: 200, border: "3px solid  #754ffe" }}
            />

            <Hidden smDown>
              <Stack
                sx={{
                  justifyContent: "center",
                  mt: "3rem",
                  gap: ".5rem",
                }}
              >
                {[1, 1, 1].map(() => {
                  return (
                    <Link to={"/"} target="_blank">
                      <Box
                        sx={{
                          display: "flex",
                          px: "3rem",
                          border: "1px solid #754ffe",
                          height: "3.2rem",
                          justifyContent: "center",
                          alignItems: "center",
                          gap: ".4rem",
                          "&:hover": {
                            backgroundColor: "#edededbd",
                          },
                        }}
                      >
                        <Typography
                          sx={{
                            textTransform: "capitalize",
                            fontWeight: "500",
                          }}
                        >
                          twitter
                        </Typography>
                      </Box>
                    </Link>
                  );
                })}
              </Stack>
            </Hidden>
          </Box>
        </Box>
      </Box>
      <Footer />
    </>
  );
};

export default InstructorProfile;

const CourseCard = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: rgba(149, 157, 165, 0.3) 0px 8px 24px;
  display: flex;
  flex-direction: column;

  background-color: #f7f7f7;

  width: 290px;
  height: 21rem;
  @media screen and (max-width: 400px) {
    width: 280px;
  }

  .item-body {
    margin: 10px 0;
    padding: 4px 18px;

    .item-name {
      font-size: 14px;
      line-height: 1.4;
      font-weight: 600;
      text-align: start;
    }
    .item-creator {
      font-size: 12.5px;
      font-weight: 500;
      color: rgba(0, 0, 0, 0.6);
    }
    .rating-star-val {
      margin-bottom: 5px;
      font-size: 14px;
      font-weight: 800;
      color: #b4690e;
      margin-right: 6px;
    }
    .rating-count {
      font-size: 12.5px;
      margin-left: 3px;
      font-weight: 500;
      opacity: 0.8;
    }
    .item-price-new {
      font-weight: 700;
      font-size: 15px;
    }
    .item-price-old {
      opacity: 0.8;
      font-weight: 500;
      text-decoration: line-through;
      font-size: 15px;
      margin-left: 8px;
    }
  }

  .item-btns {
    justify-self: flex-start;
    padding: 4px 8px 20px 18px;
    margin-top: auto;
    .item-btn {
      font-size: 15px;
      display: inline-block;
      padding: 6px 16px;
      font-weight: 700;
      transition: var(--transition);
      white-space: nowrap;

      &.see-details-btn {
        background-color: transparent;
        border: 1px solid #754ffe;
        margin-right: 5px;
        color: #754ffe;
        font-size: 0.8rem;
        font-weight: 600;

        &:hover {
          background-color: #754ffe;
          color: var(--clr-white);
        }
      }

      &.add-to-cart-btn {
        background: #754ffe;
        color: var(--clr-white);
        border: 1px solid #754ffe;
        font-size: 0.8rem;
        font-weight: 600;

        &:hover {
          background-color: transparent;
          color: #754ffe;
        }
      }
    }
  }
`;
