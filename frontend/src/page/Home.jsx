import React from "react";
import Hero from "../components/Hero";
import { comCss } from "../components/ComponentsCss";

import { pageCss } from "./PageCss";
import { Box, Button, Container, Grid, Stack, Typography } from "@mui/material";
import Slidercourse from "../components/Slidercourse";
import Mantor from "../components/Mantor";
import Blog from "../components/ArticleCard";
import TextimonialCom from "../components/TextimonialCom";
import Slider from "react-slick";
import { settings, testimonial } from "./Common";
import { Link, NavLink } from "react-router-dom";
import { home_count } from "../data";
import Course from "../components/Course";
// import courses from "../utils/data";
import { categories } from "../utils/data";
import { Theme } from "../Theme";
import Category from "../components/Category";
import design_1d from "../assets/images/design_1.jpg";
import Lottie from "react-lottie";

import { Carousel } from "primereact/carousel";

import useMediaQuery from "@mui/material/useMediaQuery";
import Footer from "../components/Footer";
import { Swiper, SwiperSlide } from "swiper/react";
import Testimonials from "./Testimonials";
//

import { useRef, useState, useEffect } from "react";
// Import Swiper React components

// components
import Loader from "../components/loader/Loader";

// Import Swiper styles
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";

import "./slider.css";

// import required modules
import { Navigation } from "swiper/modules";
import { FreeMode, Pagination } from "swiper/modules";
import axios from "axios";
import FetchFromApi from "../utils/FetchFromApi";

//
import useLocale from "../hook/useLocales";

// redux
import { useDispatch, useSelector } from "react-redux";

// store
import {
  Video_AllCourse,
  selectIsLoading,
  selectAllCourses,
} from "../store/features/video/courses/videoCoursesSlice";
// store
import {
  Video_AllCategories,
  selectVideoAllCategories,
  selectIsLoading as Category_isLoading,
} from "../store/features/video/category/videoCategorySlice";

const Home = () => {
  // redux
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading);
  const AllCourses = useSelector(selectAllCourses);
  const video_Category = useSelector(selectVideoAllCategories);
  const video_Category_IsLoading = useSelector(Category_isLoading);

  console.log("isLoading", video_Category_IsLoading);
  useEffect(() => {
    dispatch(Video_AllCourse());
    dispatch(Video_AllCategories());
  }, []);

  const { translate } = useLocale();
  const classes = pageCss();
  const comclasses = comCss();

  const [courses, setCourses] = useState([]);
  const [averageRating, setAverageRating] = useState(0);

  const matches_1180 = useMediaQuery("(max-width:1180px)");
  const matches_800 = useMediaQuery("(max-width:800px)");
  const matches_500 = useMediaQuery("(max-width:500px)");
  const matches_450 = useMediaQuery("(max-width:450px)");
  const matches_480 = useMediaQuery("(max-width:480px)");

  return (
    <>
      {isLoading && <Loader />}
      <Hero />
      {/* <!-- start your journey section --> */}

      {/* <!-- End Hero --> */}
      {/* <Extra1 /> */}

      <Box className={classes.home_course_section}>
        <Container maxWidth="lg">
          <Box
            sx={{
              paddingTop: { xs: "0", sm: "8rem" },
            }}
          >
            <Typography
              variant="h3"
              component="h3"
              sx={{
                textAlign: { xs: "center", sm: "start" },
                fontWeight: "500 !important",
                fontSize: {
                  xs: "1.4rem !important",
                  sm: "2rem !important",
                },
              }}
            >
              {translate("Courses For you")}
            </Typography>
            {/* courses row */}
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: { xs: "center", sm: "start" },
                gap: "2rem",
                height: "30rem",
              }}
            >
              <Swiper
                slidesPerView={
                  matches_1180 ? (matches_800 ? (matches_500 ? 1 : 2) : 3) : 4
                }
                spaceBetween={30}
                freeMode={true}
                pagination={{
                  clickable: true,
                }}
                modules={[FreeMode, Pagination, Navigation]}
                className="mySwiper"
              >
                {AllCourses?.map((item, idx) => {
                  if (idx >= 5) {
                    return;
                  }
                  return (
                    <SwiperSlide key={idx}>
                      <Course course={item} />
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            </Box>
          </Box>
          {/* Most Popular */}
          <Box className={classes.home_course}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: { xs: "4rem", sm: "8rem" },
              }}
            >
              <Typography
                variant="h3"
                component="h3"
                sx={{
                  textAlign: { xs: "center", sm: "start" },
                  fontWeight: "400 !important",
                  fontSize: {
                    xs: "1.4rem !important",
                    sm: "2.2rem !important",
                  },
                  mb: 1,
                }}
              >
                {translate("Most Populer")}
              </Typography>

              <Link
                to="/courses"
                style={{
                  fontSize: "1rem",
                  color: "#754ffe",
                }}
              >
                {translate("View All")}
              </Link>
            </Box>
            <Box
              sx={{
                display: "flex",
                gap: "2rem",
                height: "28rem",
                flexWrap: "wrap",
                justifyContent: { xs: "center", sm: "start" },
              }}
            >
              <Swiper
                slidesPerView={
                  matches_1180 ? (matches_800 ? (matches_500 ? 1 : 2) : 3) : 4
                }
                spaceBetween={30}
                freeMode={true}
                pagination={{
                  clickable: true,
                }}
                modules={[FreeMode, Pagination, Navigation]}
                className="mySwiper"
              >
                {courses.map((item, idx) => {
                  if (idx >= 5) {
                    return;
                  }
                  return (
                    <SwiperSlide key={idx}>
                      <Course course={item} />
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            </Box>
          </Box>
          {/* Read */}

          <Box className={classes.home_course}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: { xs: "4rem", sm: "8rem" },
              }}
            >
              <Typography
                variant="h3"
                component="h3"
                sx={{
                  textAlign: { xs: "center", sm: "start" },
                  fontWeight: "400 !important",
                  fontSize: {
                    xs: "1.4rem !important",
                    sm: "2.2rem !important",
                  },
                  mb: 7,
                }}
              >
                {translate("Read")}
              </Typography>

              <Link
                to="/courses"
                style={{
                  fontSize: "1rem",
                  color: "#754ffe",
                }}
              >
                {translate("View All")}
              </Link>
            </Box>
            <Box
              sx={{
                display: "flex",
                gap: "2rem",
                flexWrap: "wrap",
                justifyContent: { xs: "center", sm: "space-between" },
              }}
            >
              {courses.map((item, idx) => {
                if (idx >= 4) {
                  return;
                }
                return (
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                    }}
                    key={idx}
                  >
                    <div
                      className="flex max-w-lg overflow-hidden bg-white rounded-lg dark:bg-gray-800 "
                      style={{
                        backgroundColor: "rgba(218,228,237,.32)",
                        maxHeight: matches_450 ? "130px" : "160px",
                        alignItems: "start",
                      }}
                    >
                      <Box
                        style={{
                          width: matches_450 ? "60%" : "50%",
                        }}
                      >
                        <Link to={"article-details"}>
                          <img
                            style={{ minHeight: "190px", maxHeight: "190px" }}
                            src="https://media.istockphoto.com/id/1483964760/photo/woman-reading-book-on-beach-near-sea-closeup-space-for-text.webp?s=1024x1024&w=is&k=20&c=kcbCmmpLVrLgLIAmb9RnZ151Cu-ZkHBAd20qfBEcn-s="
                            alt=""
                          />
                        </Link>
                      </Box>

                      <Box
                        sx={{
                          width: "15rem",
                          px: "1rem",
                          py: "1rem",
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "space-between",
                          height: "100%",
                        }}
                      >
                        <Typography
                          style={{
                            textAlign: "start",
                            fontSize: ".9rem",
                            fontWeight: 700,
                          }}
                        >
                          <Link to={"/article-details"}>
                            What is Motion Graphics?
                          </Link>
                        </Typography>

                        <Box
                          sx={{
                            display: "flex",
                            flexDirection: "column",
                          }}
                        >
                          <Link to="/user/wer">
                            <Box sx={{ display: "flex", alignItems: "center" }}>
                              <img
                                style={{ height: "2rem", width: "2rem" }}
                                className="object-cover rounded-full"
                                src="https://images.unsplash.com/photo-1586287011575-a23134f797f9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=48&q=60"
                                alt="Avatar"
                              />
                              <Stack sx={{ ml: ".6rem", alignItems: "start" }}>
                                <Typography
                                  variant="body1"
                                  sx={{
                                    fontSize: ".8rem",
                                    fontWeight: "bold",
                                    margin: 0,
                                    color: "#754ffe",
                                    "&:hover": {
                                      color: "#a190e2",
                                    },
                                  }}
                                >
                                  Jone Doe
                                </Typography>
                                <span
                                  style={{
                                    fontSize: ".7rem",
                                    fontWeight: "bold",
                                    margin: 0,
                                    color: "gray",
                                  }}
                                >
                                  21 SEP 2015
                                </span>
                              </Stack>
                            </Box>
                          </Link>
                        </Box>
                      </Box>
                    </div>
                  </Box>
                );
              })}
            </Box>
          </Box>
          {/* Top Categories */}

          <Box
            sx={{
              gap: 5,
              mb: "1rem",
              mt: "12rem",
            }}
          >
            <Typography
              variant="h3"
              component="h3"
              sx={{
                textAlign: { xs: "center", sm: "start" },
                mt: { xs: "4rem", sm: "6rem" },
                marginBottom: { xs: "4rem", sm: "5rem" },
                fontWeight: "400 !important",
                fontSize: {
                  xs: "1.4rem !important",
                  sm: "2.2rem !important",
                },
              }}
            >
              {translate("Top Categories")}
              {video_Category_IsLoading && <Loader />}
            </Typography>
            <Box
              sx={{
                display: "flex",
                justifyContent: { xs: "center", sm: "start" },
                flexWrap: "wrap",
                gap: "2rem",
              }}
            >
              {video_Category
                ? video_Category?.map((item, idx) => {
                    return (
                      <Category name={item.name} image={item.image} key={idx} />
                    );
                  })
                : "No Category"}
            </Box>
          </Box>

          {/* testimonials section start */}
          <section className=" ">
            <div className=" mx-auto max-w-7xl ">
              <div className="flex flex-col items-center">
                <Typography
                  variant="h3"
                  component="h3"
                  sx={{
                    width: "100%",
                    textAlign: { xs: "center", sm: "start" },
                    fontWeight: "400 !important",
                    fontSize: {
                      xs: "1.4rem !important",
                      sm: "2.2rem !important",
                    },
                    mt: { xs: "2rem", sm: "6rem" },
                  }}
                >
                  {translate("Our happy clients say about us")}
                </Typography>

                <div className="text-center md:mt-16 md:order-3">
                  {/* <a
                  href="#"
                  title=""
                  className="pb-2 text-base font-bold leading-7 text-gray-900 transition-all duration-200 border-b-2 border-gray-900 hover:border-gray-600 font-pj focus:outline-none focus:ring-1 focus:ring-gray-900 focus:ring-offset-2 hover:text-gray-600"
                >
                  Check all 2,157 reviews
                </a> */}
                </div>

                <div className="relative mt-10 md:mt-24 md:order-2">
                  <div className="absolute -inset-x-1 inset-y-16 md:-inset-x-2 md:-inset-y-6">
                    <div
                      className="w-full h-full max-w-5xl mx-auto rounded-3xl opacity-30 blur-lg filter"
                      style={{
                        background:
                          "linear-gradient(90deg, #44ff9a -0.55%, #44b0ff 22.86%, #8b44ff 48.36%, #ff6644 73.33%, #ebff70 99.34%)",
                      }}
                    ></div>
                  </div>

                  <div className="relative grid max-w-lg grid-cols-1 gap-6 mx-auto md:max-w-none lg:gap-10 md:grid-cols-3">
                    <div className="flex flex-col overflow-hidden shadow-xl">
                      <div className="flex flex-col justify-between flex-1 p-6 bg-white lg:py-8 lg:px-7">
                        <div className="flex-1">
                          <div className="flex items-center">
                            {[1, 1, 1, 1, 1].map((item, idx) => {
                              return (
                                <svg
                                  key={idx}
                                  className="w-5 h-5 text-[#FDB241]"
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 20 20"
                                  fill="currentColor"
                                >
                                  <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                </svg>
                              );
                            })}
                          </div>

                          <blockquote className="flex-1 mt-8">
                            <p className="text-lg leading-relaxed text-gray-900 font-pj">
                              “You made it so simple. My new site is so much
                              faster and easier to work with than my old site. I
                              just choose the page, make the change.”
                            </p>
                          </blockquote>
                        </div>

                        <div className="flex items-center mt-8">
                          <img
                            className="flex-shrink-0 object-cover rounded-full w-11 h-11"
                            src="https://cdn.rareblocks.xyz/collection/clarity/images/testimonial/4/avatar-male-1.png"
                            alt=""
                          />
                          <div className="ml-4">
                            <p className="text-base font-bold text-gray-900 font-pj">
                              Leslie Alexander
                            </p>
                            <p className="mt-0.5 text-sm font-pj text-gray-600">
                              Freelance React Developer
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="flex flex-col overflow-hidden shadow-xl">
                      <div className="flex flex-col justify-between flex-1 p-6 bg-white lg:py-8 lg:px-7">
                        <div className="flex-1">
                          <div className="flex items-center">
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                          </div>

                          <blockquote className="flex-1 mt-8">
                            <p className="text-lg leading-relaxed text-gray-900 font-pj">
                              “Simply the best. Better than all the rest. I’d
                              recommend this product to beginners and advanced
                              users.”
                            </p>
                          </blockquote>
                        </div>

                        <div className="flex items-center mt-8">
                          <img
                            className="flex-shrink-0 object-cover rounded-full w-11 h-11"
                            src="https://cdn.rareblocks.xyz/collection/clarity/images/testimonial/4/avatar-male-2.png"
                            alt=""
                          />
                          <div className="ml-4">
                            <p className="text-base font-bold text-gray-900 font-pj">
                              Jacob Jones
                            </p>
                            <p className="mt-0.5 text-sm font-pj text-gray-600">
                              Digital Marketer
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="flex flex-col overflow-hidden shadow-xl">
                      <div className="flex flex-col justify-between flex-1 p-6 bg-white lg:py-8 lg:px-7">
                        <div className="flex-1">
                          <div className="flex items-center">
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg
                              className="w-5 h-5 text-[#FDB241]"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 20 20"
                              fill="currentColor"
                            >
                              <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                          </div>

                          <blockquote className="flex-1 mt-8">
                            <p className="text-lg leading-relaxed text-gray-900 font-pj">
                              “I cannot believe that I have got a brand new
                              landing page after getting Omega. It was super
                              easy to edit and publish.”
                            </p>
                          </blockquote>
                        </div>

                        <div className="flex items-center mt-8">
                          <img
                            className="flex-shrink-0 object-cover rounded-full w-11 h-11"
                            src="https://cdn.rareblocks.xyz/collection/clarity/images/testimonial/4/avatar-female.png"
                            alt=""
                          />
                          <div className="ml-4">
                            <p className="text-base font-bold text-gray-900 font-pj">
                              Jenny Wilson
                            </p>
                            <p className="mt-0.5 text-sm font-pj text-gray-600">
                              Graphic Designer
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </Container>

        {/* testimonials section end */}

        {/* our achievmetns */}
        <Box
          className={classes.home_count_section}
          sx={{
            mb: "4rem",
          }}
        >
          <Container maxWidth="lg">
            <Typography
              variant="h3"
              component="h3"
              sx={{
                color: "black",
                fontSize: {
                  xs: "1.4rem !important",
                  sm: "2.2rem !important",
                },
                textAlign: { xs: "center", sm: "start" },
                mb: { xs: "4rem", sm: "5rem" },
                mt: { xs: "2rem", sm: "4rem" },
              }}
            >
              {translate("Our Achievements")}
            </Typography>
            <Grid container spacing={2}>
              {home_count.map((item, index) => (
                <Grid item xs={12} sm={6} md={3} key={index}>
                  <Box
                    className={classes.count_icon_box}
                    sx={{
                      justifyContent: { xs: "center", sm: "start" },
                    }}
                  >
                    <Box className={classes.count_icon}>{item.icon}</Box>
                    <Box className={classes.count_content}>
                      <Typography
                        variant="h4"
                        component="h4"
                        className={classes.count_title}
                      >
                        {item.title}
                      </Typography>
                      <Typography
                        variant="h4"
                        component="p"
                        className={classes.count_des}
                      >
                        {item.des}
                      </Typography>
                    </Box>
                  </Box>
                </Grid>
              ))}
            </Grid>
          </Container>
        </Box>

        {/* our achievments end */}
      </Box>
      <Footer />
    </>
  );
};

export default Home;
