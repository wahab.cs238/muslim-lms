import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  loginUser,
  selectIsLoading,
  selectUserInfo,
  selectIsLoggedIn,
  selectIsAdmin,
} from "../../store/auth/user/userSlice";
import Loader from "../../components/loader/Loader";
import { toast } from "react-toastify";

const initialState = {
  email: "",
  password: "",
};

const Login = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading);
  const UserInfo = useSelector(selectUserInfo);
  const admin = useSelector(selectIsAdmin);
  console.log("admin", admin);
  const navigate = useNavigate();
  // const [isLoading, setIsLoading] = useState(false);
  const [formData, setformData] = useState(initialState);
  const { email, password } = formData;

  console.log("sddf", localStorage.getItem("IsLoggedIn"));

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setformData({ ...formData, [name]: value });
  };

  const login = async (e) => {
    e.preventDefault();

    if (!email || !password) {
      return toast.error("All fields are required");
    }

    const userData = {
      email,
      password,
    };
    // setIsLoading(true);
    try {
      const data = await dispatch(loginUser(userData));
      console.log(data.payload);
      // setIsLoading(false);
    } catch (error) {
      // setIsLoading(false);
    }
  };

  return (
    <>
      {isLoading && <Loader />}
      <div className="font-sans text-gray-900 antialiased">
        <div className="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-[#f8f4f3]">
          <div>
            <h2 className="font-bold text-3xl">
              Welcome{" "}
              <span className="bg-[#f84525] text-white px-2 rounded-md">
                Back
              </span>
            </h2>
          </div>

          <div className="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
            <form>
              <div className="py-8">
                <center>
                  <span className="text-2xl font-semibold">Log In</span>
                </center>
              </div>

              <div>
                <label
                  className="block font-medium text-sm text-gray-700"
                  for="email"
                  value="Email"
                />
                <input
                  style={{ border: "2px solid #754ffe" }}
                  type="email"
                  name="email"
                  required=""
                  value={email}
                  onChange={handleInputChange}
                  placeholder="Email"
                  className="w-full rounded-md py-2.5 px-4 border text-sm outline-[#f84525]"
                />
              </div>

              <div className="mt-4">
                <label
                  className="block font-medium text-sm text-gray-700"
                  for="password"
                  value="Password"
                />
                <div className="relative">
                  <input
                    style={{ border: "2px solid #754ffe" }}
                    id="password"
                    type="password"
                    name="password"
                    placeholder="Password"
                    value={password}
                    onChange={handleInputChange}
                    required
                    autocomplete="true"
                    className="w-full rounded-md py-2.5 px-4 border text-sm outline-[#f84525]"
                  />

                  <div className="absolute inset-y-0 right-0 pr-3 flex items-center text-sm leading-5">
                    <button
                      type="button"
                      id="togglePassword"
                      className="text-gray-500 focus:outline-none focus:text-gray-600 hover:text-gray-600"
                    ></button>
                  </div>
                </div>
              </div>

              <div className="block mt-4">
                <label for="remember_me" className="flex items-center">
                  <input
                    type="checkbox"
                    id="remember_me"
                    name="remember"
                    className="rounded border-gray-300 text-indigo-600 shadow-sm focus:ring-indigo-500"
                  />
                  <span className="ms-2 text-sm text-gray-600">
                    Remember Me
                  </span>
                </label>
              </div>

              <div className="flex items-center justify-end mt-4">
                <a
                  className="hover:underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  href="{{ route('password.request') }}"
                >
                  Forgot your password?
                </a>

                <button
                  onClick={(e) => login(e)}
                  className="ms-4 inline-flex items-center px-4 py-2 bg-[#f84525] border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-800 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150"
                >
                  Sign In
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
