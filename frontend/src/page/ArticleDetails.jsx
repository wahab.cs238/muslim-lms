import React from "react";
import Pagebanner from "../components/Pagebanner";
import { pageCss } from "./PageCss";
import blogimg from "../image/blog.jpg";
import {
  Box,
  Button,
  Container,
  Typography,
  useMediaQuery,
  Grid,
  Stack,
  Pagination,
  Rating,
} from "@mui/material";
import { Link, NavLink } from "react-router-dom";
import { comCss } from "../components/ComponentsCss";
import ArticleCard from "../components/ArticleCard";
import Footer from "../components/Footer";

const ArticleDetials = () => {
  const classes = pageCss();
  const comclasses = comCss();

  const matches_450 = useMediaQuery("(max-width:450px)");
  const data = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  return (
    <>
      {/* <Pagebanner title="Single blog page" /> */}
      <Container maxWidth="lg">
        <Box
          className={classes.blog_post_img_box}
          sx={{ position: "relative" }}
        >
          <img src={blogimg} alt="blog img" className={classes.blog_post_img} />
          <Box
            className="absolute left-0 bottom-0 w-full z-10"
            sx={{
              backgroundImage:
                "linear-gradient(180deg,transparent,rgba(0,0,0,.7))",
              height: "80%",
              borderRadius: "8px",
            }}
          ></Box>
          <div className="p-4 absolute bottom-0 left-0 z-20">
            <Link
              to={"/"}
              style={{ backgroundColor: "#754ffe", borderRadius: "4px" }}
              className="px-4 py-1 text-gray-200 inline-flex items-center justify-center mb-2"
            >
              <Typography>Nutrition</Typography>
            </Link>
          </div>
        </Box>
        <div className="p-4  bottom-0 left-0 z-20">
          <Typography
            variant="h4"
            className="text-4xl font-semibold leading-tight"
            sx={{
              fontSize: matches_450 ? ".9rem" : "2.3rem",
              fontWeight: "500",
            }}
          >
            Pellentesque a consectetur velit, ac molestie ipsum. Donec sodales,
            massa et auctor.
          </Typography>
          <Link to={"/user/123"}>
            <div className="flex mt-3">
              <img
                src="https://randomuser.me/api/portraits/men/97.jpg"
                className="h-10 w-10 rounded-full mr-2 object-cover"
              />
              <div>
                <p className="font-semibold text-sm"> Mike Sullivan </p>
                <p className="font-semibold text-gray-400 text-xs"> 14 Aug </p>
              </div>
            </div>
          </Link>
        </div>

        <Box
          className={classes.single_blog_text}
          className="max-w-4xl mx-auto px-4"
        >
          <Typography
            variant="h6"
            sx={{
              fontSize: { xs: ".8rem", sm: "1rem" },
              mt: "1rem",
              letterSpacing: "1px",
              lineHeight: "2.2rem",
            }}
          >
            If you’re learning to program for the first time, or if you’re
            coming from a different language, this course, JavaScript: Getting
            Started, will give you the basics for coding in JavaScript. First,
            you’ll discover the types of applications that can be built with
            JavaScript, and the platforms they’ll run on platforms they’ll run
            on. Condimentum leo utipsum euismod feugiatn elntum sapiennonser
            variusmi elementum necunc elementum velitnon tortor convallis
            variusa placerat nequhse. Mauris varius ullamcorper tincidsellus
            egestas innisivel sollicituullam feugiate facilisis ones Suspendisse
            blandit sedtincinean. Mauris varius ullamcorper tincidsellus egestas
            innisivel sollicituullam feugiate facilisis ones velleo finibus
            maximus nequsese sedmattis auspendisse duimetus ullamcorper
            faucibuse blandit sedtincinean. Condimentum leo utipsum euismod
            feugiatn elntum sapiennonser variusmi elementum necunc elementum
            velitnon tortor convallis variusa placerat nequhse. Mauris varius
            ullamcorper tincidsellus egestas innisivel sollicituullam feugiate
            facilisis ones Suspendisse blandit sedtincinean. Mauris varius
            ullamcorper tincidsellus egestas innisivel sollicituullam feugiate
            facilisis ones velleo finibus maximus nequsese sedmattis auspendisse
            duimetus ullamcorper faucibuse blandit sedtincinean. Condimentum leo
            utipsum euismod feugiatn elntum sapiennonser variusmi elementum
            necunc elementum velitnon tortor convallis variusa placerat nequhse.
            Mauris varius ullamcorper tincidsellus egestas innisivel
            sollicituullam feugiate facilisis ones Suspendisse blandit
            sedtincinean. Mauris varius ullamcorper tincidsellus egestas
            innisivel sollicituullam feugiate facilisis ones velleo finibus
            maximus nequsese sedmattis auspendisse duimetus ullamcorper
            faucibuse blandit sedtincinean. Condimentum leo utipsum euismod
            feugiatn elntum sapiennonser variusmi elementum necunc elementum
            velitnon tortor convallis variusa placerat nequhse. Mauris varius
            ullamcorper tincidsellus egestas innisivel sollicituullam feugiate
            facilisis ones Suspendisse blandit sedtincinean. Mauris varius
            ullamcorper tincidsellus egestas innisivel sollicituullam feugiate
            facilisis ones velleo finibus maximus nequsese sedmattis auspendisse
            duimetus ullamcorper faucibuse blandit sedtincinean. Condimentum leo
            utipsum euismod feugiatn elntum sapiennonser variusmi elementum
            necunc elementum velitnon tortor convallis variusa placerat nequhse.
            Mauris varius ullamcorper tincidsellus egestas innisivel
            sollicituullam feugiate facilisis ones Suspendisse blandit
            sedtincinean. Mauris varius ullamcorper tincidsellus egestas
            innisivel sollicituullam feugiate facilisis ones velleo finibus
            maximus nequsese sedmattis auspendisse duimetus ullamcorper
            faucibuse blandit sedtincinean. Condimentum leo utipsum euismod
            feugiatn elntum sapiennonser variusmi elementum necunc elementum
            velitnon tortor convallis variusa placerat nequhse. Mauris varius
            ullamcorper tincidsellus egestas innisivel sollicituullam feugiate
            facilisis ones Suspendisse blandit sedtincinean. Mauris varius
            ullamcorper tincidsellus egestas innisivel sollicituullam feugiate
            facilisis ones velleo finibus maximus nequsese sedmattis auspendisse
            duimetus ullamcorper faucibuse blandit sedtincinean.
          </Typography>
        </Box>
      </Container>

      {/* Add a review */}

      <section class="bg-white dark:bg-gray-900 py-8 lg:py-16 antialiased">
        <div class="max-w-2xl mx-auto px-4">
          <div class="flex justify-between items-center mb-6">
            <h2 class="text-lg lg:text-2xl font-bold text-gray-900 dark:text-white">
              Reviews (20)
            </h2>
          </div>
          <form class="mb-6">
            {/* give rating */}
            <p class=" items-center text-sm text-gray-900 dark:text-white font-semibold">
              Select Rating
            </p>
            <Rating
              name="no-value"
              value={null}
              sx={{ mb: "1.2rem", mt: "1rem" }}
            />
            <div class="py-2 px-4 mb-4 bg-white rounded-lg rounded-t-lg border border-gray-200 dark:bg-gray-800 dark:border-gray-700">
              <label for="comment" class="sr-only">
                Your comment
              </label>
              {/* give comment */}
              <textarea
                id="comment"
                rows="6"
                class="px-0 w-full text-sm text-gray-900 border-0 focus:ring-0 focus:outline-none dark:text-white dark:placeholder-gray-400 dark:bg-gray-800"
                placeholder="Write a comment..."
                required
              ></textarea>
            </div>
            <button
              type="submit"
              style={{
                backgroundColor: "#754ffe",
              }}
              className="inline-flex items-center py-2.5 px-4 text-xs font-medium text-center text-white rounded-md focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800"
            >
              Post comment
            </button>
          </form>
          <article class="p-6 text-base bg-white border-b border-gray-200 dark:border-gray-700 rounded-lg dark:bg-gray-900">
            <footer class="flex justify-between items-center mb-2">
              <div class="flex items-center">
                <p class="inline-flex items-center mr-3 text-sm text-gray-900 dark:text-white font-semibold">
                  <img
                    class="mr-2 w-6 h-6 rounded-full"
                    src="https://flowbite.com/docs/images/people/profile-picture-2.jpg"
                    alt="Michael Gough"
                  />
                  Michael Gough
                </p>
                <p class="text-sm text-gray-600 dark:text-gray-400">
                  <time
                    pubdate
                    datetime="2022-02-08"
                    title="February 8th, 2022"
                  >
                    Feb. 8, 2022
                  </time>
                </p>
              </div>
            </footer>
            <div
              className="flex items-center "
              style={{
                marginTop: ".5rem",
                marginBottom: ".5rem",
              }}
            >
              {[1, 1, 1, 1, 1].map(() => {
                return (
                  <svg
                    className="w-5 h-5 text-[#FDB241]"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                  </svg>
                );
              })}
            </div>
            <p class="text-gray-500 dark:text-gray-400">
              Very straight-to-point article. Really worth time reading. Thank
              you! But tools are just the instruments for the UX designers. The
              knowledge of the design tools are as important as the creation of
              the design strategy.
            </p>
          </article>
          <article class="p-6 text-base bg-white border-b border-gray-200 dark:border-gray-700 rounded-lg dark:bg-gray-900">
            <footer class="flex justify-between items-center mb-2">
              <div class="flex items-center">
                <p class="inline-flex items-center mr-3 text-sm text-gray-900 dark:text-white font-semibold">
                  <img
                    class="mr-2 w-6 h-6 rounded-full"
                    src="https://flowbite.com/docs/images/people/profile-picture-2.jpg"
                    alt="Michael Gough"
                  />
                  Michael Gough
                </p>
                <p class="text-sm text-gray-600 dark:text-gray-400">
                  <time
                    pubdate
                    datetime="2022-02-08"
                    title="February 8th, 2022"
                  >
                    Feb. 8, 2022
                  </time>
                </p>
              </div>
            </footer>
            <div
              className="flex items-center "
              style={{
                marginTop: ".5rem",
                marginBottom: ".5rem",
              }}
            >
              {[1, 1, 1, 1, 1].map(() => {
                return (
                  <svg
                    className="w-5 h-5 text-[#FDB241]"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                  </svg>
                );
              })}
            </div>
            <p class="text-gray-500 dark:text-gray-400">
              Very straight-to-point article. Really worth time reading. Thank
              you! But tools are just the instruments for the UX designers. The
              knowledge of the design tools are as important as the creation of
              the design strategy.
            </p>
          </article>
        </div>
      </section>

      {/* Add a review end */}

      {/* Related Articles */}

      <Box className={classes.blog_section_all} sx={{ mt: "4rem" }}>
        <Container maxWidth="lg">
          <Typography
            variant="h4"
            sx={{
              fontWeight: 700,
              mt: { xs: "3rem", sm: "0" },
              mb: { xs: "3rem" },
              fontSize: { xs: "1.4rem", sm: "2rem" },
            }}
          >
            Related Articles
          </Typography>
          <Grid container spacing={3}>
            {data.map((item, idx) => {
              if (idx >= 4) return;
              return (
                <Grid item xs={12} sm={4} key={item}>
                  <ArticleCard />
                </Grid>
              );
            })}
          </Grid>
        </Container>
      </Box>
      {/* Related Articles end */}

      {/* <!-- Footer --> */}

      <Footer />

      {/* <!-- Footer --> */}
    </>
  );
};

export default ArticleDetials;
