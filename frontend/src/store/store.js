import { configureStore } from "@reduxjs/toolkit";

// user & Admin
import UserReducer from "./auth/user/userSlice";
import AdminReducer from "./auth/admin/adminSlice";

// ARTICLES
import ArticleReducer from "./features/article/articleSlice";
import ArticleCategoryReducer from "./features/article/category/articleCategorySlice";
import ArticleReviewReducer from "./features/article/reviews/articleReviewsSlice";

// VIDEOS
import videoCategoryReducer from "./features/video/category/videoCategorySlice";
import videoCourseReducer from "./features/video/courses/videoCoursesSlice";
import videoReviewReducer from "./features/video/reviews/videoReviewsSlice";
import videoSectionReducer from "./features/video/section/videoSectionSlice";

export const store = configureStore({
  reducer: {
    // user & admin
    user: UserReducer,
    admin: AdminReducer,

    // article Reducers
    article: ArticleReducer,
    articleCategory: ArticleCategoryReducer,
    articleReview: ArticleReviewReducer,

    // videos Reducers
    videoCategory: videoCategoryReducer,
    videoCourse: videoCourseReducer,
    videoReview: videoReviewReducer,
    videoSection: videoSectionReducer,
  },
});
// redux store
