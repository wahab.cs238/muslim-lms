import axios from "axios";

const token = localStorage.getItem("token");
axios.defaults.headers.common = { Authorization: `${token}` };

const BACKEND_URL = "http://localhost:3000";

const API_URL = `${BACKEND_URL}/reviews/`;

// Video- Create Review
const Video_CreateReview = async (courseId) => {
  const response = await axios.post(`${API_URL}/` + courseId);
  return response.data;
};

// Video- Delete Review
const Video_DeleteReview = async (reviewId) => {
  const response = await axios.delete(`${API_URL}/delete` + reviewId);
  return response.data;
};

// Video- GET Review
const Video_GetReview = async (reviewId) => {
  const response = await axios.delete(`${API_URL}/` + reviewId);
  return response.data;
};

// Video -Update Review
const Video_UpdateReview = async (reviewId, formData) => {
  const response = await axios.put(`${API_URL}/update/${reviewId}`, formData);
  return response.data;
};

const VideoReviewServices = {
  Video_CreateReview,
  Video_DeleteReview,
  Video_GetReview,
  Video_UpdateReview,
};

export default VideoReviewServices;
