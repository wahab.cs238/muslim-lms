import axios from "axios";

const token = localStorage.getItem("token");
axios.defaults.headers.common = { Authorization: `${token}` };

const BACKEND_URL = "http://localhost:3000";

const API_URL = `${BACKEND_URL}/articles/`;

// Article- Create Review
const ArticleCreateReview = async (ArticleId) => {
  const response = await axios.post(`${API_URL}/` + ArticleId);
  return response.data;
};

// Article- Delete Review
const ArticleDeleteReview = async (reviewId) => {
  const response = await axios.delete(`${API_URL}/` + reviewId);
  return response.data;
};

// Article -Update Review
const ArticleUpdateReview = async (reviewId, formData) => {
  const response = await axios.put(`${API_URL}/update/${reviewId}`, formData);
  return response.data;
};

const ArticleReviewService = {
  ArticleCreateReview,
  ArticleDeleteReview,
  ArticleUpdateReview,
};

export default ArticleReviewService;
