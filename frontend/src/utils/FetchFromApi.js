import axios from "axios";

const BASE_URL = "http://localhost:3000";

const FetchFromApi = async (query) => {
  const { data } = await axios.get(`${BASE_URL}/${query}`, {
    headers: {
      Authorization:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY2MjVlYjNmYzg3ODQyMTRlZTJkYTJkNCIsImlhdCI6MTcxMzc2MTEwMywiZXhwIjoxNzE0MDIwMzAzfQ.PePJublsDHbkBt50Nuy24Fk4X1kSDZ21Siqr6elCR8o",
    },
  });

  return data;
};
 
 
 
 
export default FetchFromApi 