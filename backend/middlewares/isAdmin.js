import User from "../models/User.js";

const isAdmin = async (req, res, next) => {
  console.log(req?.userAuthId);
  const user = await User.findById(req?.userAuthId);
  console.log(user);
  if (user?.role !== "admin") {
    res?.status(400)?.end({ message: "Access denied. Admin only." });
    return;
  }
  return next();
};

export default isAdmin;
