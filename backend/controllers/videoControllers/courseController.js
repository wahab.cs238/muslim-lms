import Course from "../../models/videoModels/Course.js";
import Category from "../../models/videoModels/Category.js";
import Section from "../../models/videoModels/Section.js";
import User from "../../models/User.js";
import Reivew from "../../models/videoModels/Review.js";
import isNotAdmin from "../../middlewares/isNotAdmin.js";

export const createCourse = async (req, res) => {
  const { title, description, category, thumbnail } = req.body;

  const user = await User.findById(req.userAuthId);
  // give error if one of the field is empty
  if (!title || !description || !category || !thumbnail) {
    return res.status(400).json({
      error: "Please add all fields",
    });
  }
  // check if category exist in our databse
  const categoryExist = await Category.findOne({ name: category });

  if (!categoryExist) {
    return res.status(400).json({
      error: "this category don't exist please add category first.",
    });
  }

  // create a new course
  const course = await Course.create({
    title,
    description,
    category,
    instructor: req.userAuthId,
    thumbnail,
  });

  // add course in user courses array

  user?.courses?.push(course._id);
  await user.save();

  categoryExist?.courses?.push(course._id);
  await categoryExist.save();

  res.status(201).json({
    success: true,
    course,
    message: "Course created successfully. You can add Sections now.",
  });
};

export const getAllCourses = async (req, res, next) => {
  let coursesQuery = Course.find({});
  if (req.query.title) {
    coursesQuery = coursesQuery.find({
      title: { $regex: req.query.title, $options: "i" },
    });
  }
  if (req.query.category) {
    coursesQuery = coursesQuery.find({
      category: { $regex: req.query.category, $options: "i" },
    });
  }

  const allCourses = await coursesQuery
    .select("-sections")
    .populate("instructor")
    .populate("reviews");

  res.status(200).json({
    success: true,
    allCourses,
  });
};

export const courseDetails = async (req, res) => {
  const id = req.params.id;
  const course = await Course.findById(id)
    .populate("sections")
    .populate({
      path: "reviews", // Populate the reviews field
      populate: {
        path: "user", // Populate the user field within comments
      },
    })
    .populate({
      path: "instructor",
      populate: {
        path: "courses",
        populate: {
          path: "reviews",
        },
      },
    });

  // count the average rating for instructor courses
  let averageRating = 0;

  course?.instructor?.courses?.forEach((course) => {
    course?.reviews?.forEach((review) => {
      averageRating += review?.rating;
    });
  });

  // total views
  let totalViews = 0;

  course?.instructor?.courses?.forEach((course) => {
    totalViews += course?.views;
  });

  if (!course) {
    return res.status(404).json({
      error: "Course not found.",
    });
  }

  let relatedCourses = await Course.find({
    category: course?.category,
  });

  let newRelatedCourses = relatedCourses?.filter((relateCourse) => {
    return relateCourse?.id !== course?.id;
  });

  // add veiws when a user request for course details page but when admin request for course details page views can't be added to the course
  if (await isNotAdmin(req)) {
    course.views += 1;
    await course.save();
  }

  res.status(200).json({
    course,
    totalViews,
    averageRating,
    newRelatedCourses,
  });
};

export const deleteCourse = async (req, res) => {
  const { id } = req.params;

  const course = await Course.findById(id);
  if (!course) {
    return res.status(404).json({
      error: "Course not found.",
    });
  }
  // delete all the sections in the course
  for (let i = 0; i < course?.sections?.length; i++) {
    await Section.findByIdAndDelete(course?.sections[i]?._id);
  }
  // delete all the reviews of this course
  for (let i = 0; i < course?.reviews?.length; i++) {
    await Review.findByIdAndDelete(course?.reviews[i]?._id);
  }

  // find this course in users courses array
  const user = await User.findById(course?.instructor);

  //  in this line we finded the user to who this course belongs now we well delete this course
  const newCourses = user?.courses?.filter(
    (courseId) => courseId?.toString() !== course?._id.toString()
  );

  user.courses = newCourses;
  await user.save();
  await course.deleteOne();

  res.status(200).json({
    success: true,
    course,
    message: "Course deleted successfully.",
  });
};

// update Course
export const updateCourse = async (req, res) => {
  const courseId = req.params.courseId;
  const thumbnail = req?.file?.path;
  const { title, description, category } = req.body;
  try {
    const course = await Course.findById(courseId);
    // check if category exist in our databse
    let newCategoryExist;
    if (category) {
      newCategoryExist = await Category.findOne({ name: category });

      if (!newCategoryExist) {
        return res.status(200).json({
          error: "this category don't exist please add category first.",
        });
      }
    }

    // check if we have the course in our database
    if (!course) {
      throw new Error("Course Not Found.");
    }

    // first we should delete this course from the old category
    const oldCategoryExist = await Category.findOne({
      name: course?.category,
    });

    const newCategories = oldCategoryExist?.courses?.filter(
      (courseId) => courseId?.toString() !== course?._id?.toString()
    );

    oldCategoryExist.courses = newCategories;

    await oldCategoryExist.save();

    // then we shound enter this course to the new cateogry course array
    newCategoryExist?.courses?.push(course?._id);
    await newCategoryExist?.save();

    if (title) {
      course.title = title;
    }
    if (description) {
      course.description = description;
    }
    if (category) {
      course.category = category;
    }
    if (thumbnail) {
      course.thumbnail = thumbnail;
    }
    await course.save();

    return res.json({
      success: "Course Updated.",
      course,
    });
  } catch (error) {
    res.status(404).json({ error: error.message });
  }
  //
};
