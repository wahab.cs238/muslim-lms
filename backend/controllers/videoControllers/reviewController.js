import Review from "../../models/videoModels/Review.js";
import Course from "../../models/videoModels/Course.js";

export const createReview = async (req, res) => {
  const { rating, comment } = req.body;
  try {
    // find the course and if course finded populate all the reviews
    const courseExist = await Course.findById(req.params.courseId).populate(
      "reviews"
    );
    if (!courseExist) {
      throw new Error("Course not found.");
    }

    // one person can add only one review
    const hasReviewed = courseExist?.reviews?.find((review) => {
      return review?.user.toString() === req?.userAuthId.toString();
    });

    if (hasReviewed) {
      throw new Error("Course has already been reviewed");
    }

    const createdReview = await Review.create({
      comment,
      rating,
      course: courseExist?._id,
      user: req?.userAuthId,
    });
    // resave id of the review in this product
    courseExist.reviews.push(createdReview._id);
    await courseExist.save();

    const newCourseExist = await Course.findById(
      req?.params?.courseId
    ).populate("reviews");

    res.status(201).json({
      createdReview,
      newCourseExist,
      message: "review created successfull.",
    });
  } catch (error) {
    res.json({
      error: error.message,
    });
  }
};

// Delete the review
export const deleteReview = async (req, res) => {
  const reviewId = req.params.reviewId;
  const review = await Review.findById(reviewId);
  // check if this is the user who created this review
  if (!review) {
    return res.status(404).json({
      error: "Review not found.",
    });
  }
  if (review?.user?.toString() !== req?.userAuthId?.toString()) {
    return res.status(404).json({
      error: "You can't delete this review",
    });
  }
  // if this user was a true user then delete the review
  const DeleteReview = await Review.findByIdAndDelete(reviewId);

  return res.status(200).json({
    success: true,
    DeleteReview,
    message: "Review Deleted Successfully.",
  });
};

// update the reviews
export const updateReview = async (req, res) => {
  const reviewId = req.params.reviewId;
  const { comment, rating } = req.body;
  const review = await Review.findById(reviewId);
  // check if this is the user who created this review
  console.log(review?.user);
  console.log(req.userAuthId);
  if (!review) {
    return res.status(404).json({
      error: "Review not found.",
    });
  }
  if (review?.user?.toString() !== req?.userAuthId?.toString()) {
    return res.status(404).json({
      error: "You can't edit this review",
    });
  }
  // if this user was a true user then delete the review
  const updateReview = await Review.findByIdAndUpdate(
    reviewId,
    {
      comment,
      rating,
    },
    {
      new: true,
    }
  );

  return res.status(200).json({
    success: true,
    updateReview,
    message: "Review Deleted Successfully.",
  });
};

// get the reviews
export const getReview = async (req, res) => {
  const reviewId = req.params.reviewId;
  const review = await Review.findById(reviewId).populate("user");
  // check if this is the user who created this review
  if (!review) {
    return res.status(404).json({
      error: "Review not found.",
    });
  }

  return res.status(200).json({
    success: true,
    review,
    message: "Review Deleted Successfully.",
  });
};
