import Category from "../../models/videoModels/Category.js";

// create a category
export const createCategory = async (req, res) => {
  const { name } = req.body;
  const image = req?.file?.path;
  // check if category exist or not
  const categoryExist = await Category.findOne({ name });
  if (categoryExist) {
    return res.status(400).json({
      error: "Category already exist.",
    });
  }
  const category = await Category.create({
    name: name?.toLowerCase(),
    user: req.userAuthId,
    image: image ? image : "test",
  });

  res.status(201).json({
    message: "category created successfully.",
    category,
  });
};

// get all the categories
export const getAllCategories = async (req, res) => {
  // check if category exist or not
  const allCategoreis = await Category.find();
  if (!allCategoreis) {
  }
  res.status(201).json({
    message: "all categories fetched successfully.",
    allCategoreis,
  });
};

// get single category by id
export const getSingleCategory = async (req, res) => {
  const id = req.params.id;
  const category = await Category.findById(id).populate("courses");

  // check if category not found
  if (!category) {
    return res.status(404).json({
      message: "Category not found.",
    });
  }

  // if category founded then return it G
  res.status(404).json({
    message: "Category found.",
    category,
  });
};

// delete only one category by id
export const deleteCategory = async (req, res) => {
  const category = await Category.findByIdAndDelete(req.params.id);
  if (!category) {
    return res.status(400).json({
      error: "no category found.",
    });
  }
  res.json({
    category,
    message: "category deleted successfull.",
  });
};
// update only one category by id
export const updateCategory = async (req, res) => {
  try {
    const category = await Category.findByIdAndUpdate(
      req.params.id,
      { name: req?.body?.name?.toLowerCase() },
      { new: true }
    );

    if (!category) {
      throw new Error("Category not found.");
    }

    res.status(201).json({
      message: "category updated successfully.",
      category,
    });
  } catch (error) {
    res.status(400).json({
      error: error.message,
    });
  }
};
