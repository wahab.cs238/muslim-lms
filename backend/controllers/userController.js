import User from "../models/User.js";
import Course from "../models/videoModels/Course.js";
import dotenv from "dotenv";
dotenv.config();
import sgMail from "@sendgrid/mail";
import bCrypt from "bcrypt";
import generateToken from "../utils/generateToken.js";
import verifyToken from "../utils/verifyToken.js";
import getTokenFromHeader from "../utils/getTokenFromHeader.js";
import Section from "../models/videoModels/Section.js";
import Article from "../models/articleModels/Article.js";
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

export const register = async (req, res) => {
  const avatar = req?.file?.path;
  const { name, email, password, role } = req.body;

  if (!name || !email || !password) {
    return res.status(400).json({
      error: "Please fill all the fields",
    });
  }
  //   check if user exist
  const isUserExist = await User.findOne({ email });
  if (isUserExist) {
    return res.status(400).json({
      error: "User already exists.",
    });
  }

  // hash the passwords
  const salt = await bCrypt.genSalt(10);
  const hashPassword = await bCrypt.hash(password, salt);
  // save the user

  const newUser = await User.create({
    name,
    email,
    password: hashPassword,
    avatar: avatar ? avatar : "Avatar",
    role: role ? role : "user",
  });
  return res.status(201).json({
    user: newUser,
    message: "User Registered Successfull",
  });
};

// login
export const login = async (req, res) => {
  const { email, password } = req.body;
  // check if user is register in our database
  const user = await User.findOne({ email });
  if (!user) {
    return res.status(400).json({
      error: "Invalid login details",
    });
  }
  // check if passwords match

  const isMatch = await bCrypt.compare(password, user.password);
  if (!isMatch) {
    return res.status(400).json({
      error: "Email and password do not match",
    });
  }

  user.password = undefined;
  res.json({
    user,
    token: generateToken(user._id),
    message: "Successfull login",
  });
};

// logout
export const logout = async (req, res) => {};

// get admin profile
export const adminProfile = async (req, res) => {
  const user = await User.findById(req.userAuthId).populate("courses");

  let totalViews = 0;
  user?.courses?.forEach((course) => {
    totalViews += course?.views;
  });

  if (!user) {
    return res.json({
      error: "User not found.",
    });
  }
  res.json({ user, totalViews });
};

//
//
// get user profile
export const getMyProfile = async (req, res) => {
  const token = getTokenFromHeader(req);
  console.log(token);
  const userId = verifyToken(token);
  console.log(userId);

  const user = await User.findById(userId.id).select({
    password: 0,
    courses: 0,
  });
  if (!user) {
    return res.status(400).json({
      error: "User not found.",
    });
  }
  res.json({ user, message: "user profile" });
};

export const instructorProfile = async (req, res) => {
  const id = req.params.id;
  console.log(id);

  const user = await User.findById(id)
    .select({
      password: 0,
    })
    .populate("courses");

  return res.json({ user, message: "Instructor profile" });
};

export const changePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword || !newPassword) {
    return res.status(400).json({ error: "Please fill all fields" });
  }
  const user = await User.findById(req.userAuthId).select("+password");

  if (!user) {
    return res.status(400).json({
      error: "please login first",
    });
  }

  const oldPasswordMatch = await bCrypt.compare(oldPassword, user?.password);

  if (!oldPasswordMatch) {
    return res.status(400).json({
      error: "Incorrect old password",
    });
  }

  const salt = await bCrypt.genSalt(10);
  const hashPassword = await bCrypt.hash(newPassword, salt);
  user.password = hashPassword;
  await user.save();
  res.status(200).json({
    success: true,
    message: "Password changed successfully",
  });
};

export const updateProfile = async (req, res) => {
  const { name, email } = req.body;

  const isUserExist = await User.findOne({ email });

  if (isUserExist) {
    return res.status(400).json({
      error: "You can't use this email",
    });
  }

  const user = await User.findById(req.userAuthId);

  if (name) user.name = name;
  if (email) user.email = email;

  await user.save();

  res.status(200).json({
    success: true,
    message: "Profile updated successfully",
    user,
  });
};

export const updateProfilePicture = async (req, res) => {
  const avatar = req?.file?.path;
  if (!avatar) {
    return res.status(400).json({
      message: "Please select an image",
    });
  }
  // find the user that want to update profile pricture
  const user = await User.findById(req.userAuthId);

  user.avatar = avatar;
  await user.save();
  res.status(200).json({
    success: true,
    message: "Profile picture updated successfully",
    user,
  });
};

export const forgetPassword = async (req, res) => {
  const { email } = req.body;
  console.log(email);

  const user = await User.findOne({ email });
  console.log(user.email);
  if (!user) {
    return res.status(400).json({
      error: "User not found",
    });
  }
  const newToken = generateToken(user._id);

  //send token via email

  const msg = {
    from: "raqibyoon2020@gmail.com",
    to: user.email,
    subject: "Reset password",
    html: `
    <p>Please use the following link to activate your account</p>
    <p>http://localhost:5173/users/resetpassword/${newToken}</p>
    <hr/>

    <p>This email contain sensetive information</p>
    <p>http://localhost:5173</p>
    `,
  };

  await sgMail
    .send(msg)
    .then((data) => {
      res.status(200).json({
        message: `Email has been sent to ${email} to reset your password.`,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(400).json({
        error: "Something went wrong email not send.",
        err,
      });
    });
};

export const resetPassword = async (req, res) => {
  console.log("reset password");
  const { token } = req.params;
  console.log(token);
  const { id } = verifyToken(token);
  console.log(id);

  const foundUser = await User.findById(id);

  if (!foundUser) {
    return res.status(400).json({
      error: "Token is invalid or has been expired",
    });
  }

  const salt = await bCrypt.genSalt(12);
  const hashPassword = await bCrypt.hash(req.body.password, salt);
  foundUser.password = hashPassword;
  await foundUser.save();

  res.status(200).json({
    success: true,
    message: "Password changed successfully",
  });
};

export const addToPlaylist = async (req, res) => {
  const courseId = req.params.courseId;
  const user = await User.findById(req.userAuthId);
  const course = await Course.findById(courseId);

  if (!user) {
    return res.status(404).json({
      error: "invalid login details. please login first",
    });
  }
  if (!course) {
    return res.status(404).json({
      error: "Invalid course id",
    });
  }

  const itemAlreadyExists = user?.playlist?.find((item) => {
    if (item.toString() === course._id.toString()) {
      return true;
    }
    console.log(course);
  });

  if (itemAlreadyExists) {
    return res.status(400).json({
      error: "Course already added to playlist",
    });
  }

  user.playlist.push(course._id);
  await user.save();

  res.status(200).json({
    success: true,
    user,
    message: "Added to playlist",
  });
};

export const removeFromPlaylist = async (req, res) => {
  const { courseId } = req.params;
  const user = await User.findById(req.userAuthId);
  const course = await Course.findById(courseId);
  if (!course) {
    return res.status(400).json({
      error: "Invalid course id",
    });
  }
  if (!user) {
    return res.status(400).json({
      error: "please login first",
    });
  }

  const newPlaylist = user.playlist.filter(
    (item) => item.toString() !== course._id.toString()
  );

  user.playlist = newPlaylist;

  await user.save();

  res.status(200).json({
    success: true,
    message: "Course removed from playlist",
  });
};

export const getAllUsers = async (req, res) => {
  const users = await User.find({});
  res.status(200).json({
    users,
    success: true,
    users,
  });
};

export const updateUserRole = async (req, res) => {
  const { id } = req.params;
  const user = await User.findById(id);
  if (!user) {
    return res.status(400).json({
      error: "User not found",
    });
  }
  if (user.role === "admin") {
    user.role = "user";
  } else {
    user.role = "admin";
  }

  await user.save();

  res.status(200).json({
    user,
    success: true,
    message: "Role updated",
  });
};

export const deleteUser = async (req, res) => {
  const { userId } = req.params;
  // find the user that we want to delete
  const user = await User.findById(id);

  if (!user) {
    return res.status(404).json({
      error: "User not found",
    });
  }
  // delete all courses in user profile
  user.deleteOne();

  res.status(200).json({
    success: true,
    message: "User deleted successfully",
    user,
  });
};

export const deleteMyProfile = async (req, res) => {
  const user = await User.findById(req.userAuthId).populate("courses");

  if (!user) {
    return res.status(404).json({
      error: "User not found.",
    });
  }

  for (let i = 0; i < user?.courses?.length; i++) {
    for (let j = 0; j < user?.courses[i]?.sections?.length; j++) {
      await Section.findByIdAndDelete(user?.courses[i]?.sections[j]?._id);
    }
    await Course.findByIdAndDelete(user?.courses[i]?._id);
  }

  await user.save();
  await user.deleteOne();

  res.status(200).json({
    user,
    success: true,
    message: "User deleted successfully",
  });
};

export const getPlaylist = async (req, res) => {
  const user = await User.findById(req.userAuthId).populate("playlist");
  if (!user) {
    return res.status(404).json({
      error: "Please login first",
    });
  }
  const playlist = user?.playlist;
  res.status(200).json({
    playlist,
    message: "Play list Founded",
  });
};

// =================================================
// =======  articls playlist        ================
// =================================================

export const addArticleToPlaylist = async (req, res) => {
  const articleId = req.params.articleId;
  const user = await User.findById(req.userAuthId);
  const article = await Article.findById(articleId);

  if (!user) {
    return res.status(404).json({
      error: "invalid login details. please login first",
    });
  }
  if (!article) {
    return res.status(404).json({
      error: "Invalid article id",
    });
  }

  const itemAlreadyExists = user?.articlesPlaylist?.find((item) => {
    if (item.toString() === article._id.toString()) {
      return true;
    }
  });

  if (itemAlreadyExists) {
    return res.status(400).json({
      error: "Article already added to playlist",
    });
  }

  user?.articlesPlaylist?.push(article._id);
  await user.save();

  res.status(200).json({
    success: true,
    user,
    message: "Added to playlist",
  });
};

export const removeArticleFromPlaylist = async (req, res) => {
  const { articleId } = req.params;
  const user = await User.findById(req.userAuthId);
  const article = await Article.findById(articleId);
  if (!article) {
    return res.status(400).json({
      error: "Invalid article id",
    });
  }
  if (!user) {
    return res.status(400).json({
      error: "please login first",
    });
  }

  const newPlaylist = user?.articlesPlaylist?.filter(
    (item) => item.toString() !== article._id.toString()
  );

  user.articlesPlaylist = newPlaylist;

  await user.save();

  res.status(200).json({
    success: true,
    message: "Article removed from playlist",
    user,
  });
};

export const getArticlePlaylist = async (req, res) => {
  const user = await User.findById(req.userAuthId).populate("articlesPlaylist");
  if (!user) {
    return res.status(404).json({
      error: "Please login first",
    });
  }
  const articlesPlayList = user?.articlesPlaylist;

  res.status(200).json({
    message: "Play list Founded",
    articlesPlayList,
  });
};
