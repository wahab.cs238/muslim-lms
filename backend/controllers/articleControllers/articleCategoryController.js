import ArticleCategory from "../../models/articleModels/articleCategory.js";

// create a category
export const articleCreateCategory = async (req, res) => {
  const { name } = req.body;
  const image = req?.file?.path;
  // check if category exist or not
  const categoryExist = await ArticleCategory.findOne({ name });
  if (categoryExist) {
    return res.status(400).json({
      error: "Category already exist.",
    });
  }
  const category = await ArticleCategory.create({
    name: name?.toLowerCase(),
    user: req.userAuthId,
    image,
  });

  res.status(201).json({
    message: "category created successfully.",
    category,
  });
};

// get all the categories
export const articleGetAllCategories = async (req, res) => {
  // check if category exist or not
  const allCategoreis = await ArticleCategory.find();
  if (!allCategoreis) {
    return res.status(400).json({
      error: "no category exist.",
    });
  }
  res.status(201).json({
    message: "all categories fetched successfully.",
    allCategoreis,
  });
};

// get single category by id
export const articleGetSingleCategory = async (req, res) => {
  const id = req.params.id;
  const category = await ArticleCategory.findById(id).populate("articles");

  // check if category not found
  if (!category) {
    return res.status(404).json({
      message: "Category don't exist. please add first.",
    });
  }

  // if category founded then return it

  res.status(404).json({
    message: "Category found.",
    category,
  });
};

// delete only one category by id
export const articleDeleteCategory = async (req, res) => {
  try {
    const category = await ArticleCategory.findByIdAndDelete(req.params.id);
    if (!category) {
      return new error();
    } else {
      res.json({
        category,
        message: "category deleted successfull.",
      });
    }
  } catch (error) {
    return res.status(400).json({
      error: "no category found.",
    });
  }
};
// update only one category by id
export const articleUpdateCategory = async (req, res) => {
  try {
    const category = await ArticleCategory.findOne({ name: req.body.name });
    //
    if (category) {
      throw new Error("Category already exist.");
    }

    const updateCategory = await ArticleCategory.findByIdAndUpdate(
      req.params.id,
      { name: req?.body?.name?.toLowerCase() },
      { new: true }
    );

    if (!updateCategory) {
      throw new Error("Category not found.");
    }

    res.status(201).json({
      message: "category updated successfully.",
      updateCategory,
    });
  } catch (error) {
    res.status(400).json({
      error: error.message,
    });
  }
};
