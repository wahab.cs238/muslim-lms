import ArticleReview from "../../models/articleModels/ArticleReview.js";
import Article from "../../models/articleModels/Article.js";

export const articleCreateReview = async (req, res) => {
  const { rating, comment } = req.body;
  try {
    // find the course and if course finded populate all the reviews
    const articleExist = await Article.findById(req.params.articleId).populate(
      "reviews"
    );
    if (!articleExist) {
      throw new Error("Article not found.");
    }

    // one person can add only one review
    const hasReviewed = articleExist?.reviews?.find((review) => {
      return review?.user.toString() === req?.userAuthId.toString();
    });

    if (hasReviewed) {
      throw new Error("Article has already been reviewed");
    }

    const createdReview = await ArticleReview.create({
      comment,
      rating,
      article: articleExist._id,
      user: req.userAuthId,
    });
    // resave id of the review in this product
    articleExist.reviews.push(createdReview._id);
    await articleExist.save();

    const newArticleExist = await Article.findById(req.params.articleId).populate(
      "reviews"
    );

    res.status(201).json({
      createdReview,
      newArticleExist,
      message: "review created successfull.",
    });
  } catch (error) {
    res.status(400).json({
      error: error.message,
    });
  }
};

// Delete the review
export const articleDeleteReview = async (req, res) => {
  const reviewId = req.params.reviewId;
  const review = await ArticleReview.findById(reviewId);
  // check if this is the user who created this review
  if (!review) {
    return res.status(404).json({
      error: "Review not found.",
    });
  }
  if (review?.user?.toString() !== req?.userAuthId?.toString()) {
    return res.status(404).json({
      error: "You can't delete this review",
    });
  }
  // if this user was a true user then delete the review
  const DeleteReview = await ArticleReview.findByIdAndDelete(reviewId);

  return res.status(200).json({
    success: true,
    DeleteReview,
    message: "Review Deleted Successfully.",
  });
};

// update the reviews
export const articleUpdateReview = async (req, res) => {
  const reviewId = req.params.reviewId;
  const { comment, rating } = req.body;
  const review = await ArticleReview.findById(reviewId);
  // check if this is the user who created this review
  if (!review) {
    return res.status(404).json({
      error: "Review not found.",
    });
  }
  if (review?.user?.toString() !== req?.userAuthId?.toString()) {
    return res.status(404).json({
      error: "You can't edit this review",
    });
  }
  // if this user was a true user then delete the review
  const updateReview = await ArticleReview.findByIdAndUpdate(
    reviewId,
    {
      comment,
      rating,
    },
    {
      new: true,
    }
  );

  return res.status(200).json({
    success: true,
    updateReview,
    message: "Review Updated Successfully.",
  });
};
