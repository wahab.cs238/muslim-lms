import mongoose, { mongo } from "mongoose";
import Review from "./Review.js";

const courseSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Please enter course title"],
      maxLength: [80, "Title can't exceed 80 characters"],
    },
    description: {
      type: String,
      required: [true, "Please enter course title"],
      minLength: [20, "Description must be at least 20 characters"],
    },
    thumbnail: {
      type: String,
      required: true,
    },
    instructor: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    sections: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Section",
      },
    ],
    category: {
      type: String,
      ref: "Category",
      required: true,
    },
    views: {
      type: Number,
      default: 0,
    },
    reviews: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Review",
      },
    ],
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
  }
);

courseSchema.virtual("totalReviews").get(function () {
  const course = this;
  return course?.reviews?.length;
});

// return all the average ratings



const Course = mongoose.model("Course", courseSchema);
export default Course;
