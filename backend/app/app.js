import express from "express";
const app = express();
import dotenv from "dotenv";
dotenv.config();
import cors from "cors";

import dbConnect from "../config/dbConnection.js";
// video courses routes import
import {
  userRoutes,
  courseRoutes,
  categoryRoutes,
  reviewRoutes,
  sectionRoutes,
} from "../routes/videoRoutes/index.js";

// article routes import

import {
  articleRoutes,
  articleCategoryRoutes,
  articleReviewRoutes,
} from "../routes/articleRoutes/index.js";

// Database Connect...
dbConnect();

// middlewares
app.use(express.json());
app.use(cors());

// routes
// video courses routing part
app.use("/", userRoutes);
app.use("/courses", courseRoutes);
app.use("/categories", categoryRoutes);
app.use("/reviews", reviewRoutes);
app.use("/sections", sectionRoutes);
app.use("/categories", categoryRoutes);

// articles routing part
app.use("/articles", articleRoutes);
app.use("/articles/categories/", articleCategoryRoutes);
app.use("/articles/reviews", articleReviewRoutes);

export default app;
