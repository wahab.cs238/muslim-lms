import {
  articleCreateReview,
  articleDeleteReview,
  articleUpdateReview,
} from "../../controllers/articleControllers/articleReviewController.js";
import isLoggedIn from "../../middlewares/isLoggedIn.js";
import express from "express";
const reviewRoutes = express.Router();

reviewRoutes.post("/:articleId", isLoggedIn, articleCreateReview);
reviewRoutes.delete("/delete/:reviewId", isLoggedIn, articleDeleteReview);
reviewRoutes.put("/update/:reviewId", isLoggedIn, articleUpdateReview);

export default reviewRoutes;
